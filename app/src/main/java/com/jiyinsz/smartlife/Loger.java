package com.jiyinsz.smartlife;

import android.util.Log;

import static android.text.TextUtils.isEmpty;

/**
 * Created by varx on 9/14/15.
 */
public class Loger {

    private static final String defaultTag = "";

    public static void e(String msg) {
        if (!isEmpty(msg)&&BuildConfig.DEBUG)
            Log.e(defaultTag, msg);
    }
}
