package com.jiyinsz.smartlife.pojo;

import android.text.TextUtils;

import java.io.Serializable;

/**
 * Created by varx on 9/17/15.
 */
public class Device implements Serializable {
    public String hardware_version;
    public String firmware_version;
    public String software_version;
    public String sn_number;
    public String manufacturer_name;
    public String device_type;
    public int battery;

    public void clear() {
        hardware_version = "";
        firmware_version = "";
        software_version = "";
        sn_number = "";
        battery = 0;
    }

    public boolean isEmpty() {
        return TextUtils.isEmpty(sn_number);
    }
}
