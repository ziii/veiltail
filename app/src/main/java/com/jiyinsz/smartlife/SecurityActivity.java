package com.jiyinsz.smartlife;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jiyinsz.smartlife.service.Define;
import com.umeng.analytics.MobclickAgent;

public class SecurityActivity extends AppCompatActivity {

    private TextView mBtnSwitch;
    private View mBackground;
    private TextView mTextStatus;
    private TextView mTextStatusShort;
    private ImageView mImgCircle;
    private ImageView mImgInCircle;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_security);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("保护状态");
        mBtnSwitch = (TextView) findViewById(R.id.btn_security);
        mBackground = findViewById(R.id.security_bg);
        mTextStatus = (TextView) findViewById(R.id.text_status);
        mTextStatusShort = (TextView) findViewById(R.id.text_status_short);

        mImgInCircle = (ImageView) findViewById(R.id.img_in_circle);
        mImgCircle = (ImageView) findViewById(R.id.img_circle);

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Define.ACTION.DEVICE_DISCONNECT);
        intentFilter.addAction(Define.ACTION.DEVICE_CONNECT);
        registerReceiver(mReceiver, intentFilter);

        updateStatus();
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (!App.isSecurityOn) {
                return;
            }
            updateStatus();
        }
    };

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mReceiver);
    }

    private void updateStatus() {
        if (App.isSecurityOn) {
            mBtnSwitch.setSelected(true);
            mBtnSwitch.setText("结束保护");
            if (App.isConnect) {
                mTextStatus.setText("宝贝目前非常安全");
                mTextStatusShort.setText("安全");
                mBackground.setSelected(false);

                mImgCircle.setImageResource(R.mipmap.circle_safety);
                mImgInCircle.setImageResource(R.mipmap.protect_white);
            } else {
                mTextStatus.setText("糟糕,你的宝贝走远了");
                mTextStatusShort.setText("未开启");
                mTextStatusShort.setText("危险");
                mBackground.setSelected(true);
                mImgCircle.setImageResource(R.mipmap.circle_dangerous);
                mImgInCircle.setImageResource(R.mipmap.warning);
            }
        } else {
            mBackground.setSelected(false);
            mBtnSwitch.setSelected(true);
            mBtnSwitch.setText("开启保护");
            mTextStatus.setText("设备未开启");
            mTextStatusShort.setText("未开启");

            mImgCircle.setImageResource(R.mipmap.circle_safety);
            mImgInCircle.setImageResource(R.mipmap.protect_white);
        }
    }

    public void securitySwitch(View v) {
        //
        if (!App.isConnect && !App.isSecurityOn) {
            Toast.makeText(this, "设备未连接", Toast.LENGTH_SHORT).show();
            return;
        }
        App.isSecurityOn = !App.isSecurityOn;
        updateStatus();
        if (App.isSecurityOn) {
            mBtnSwitch.setSelected(true);
            sendBroadcast(new Intent(Define.ACTION.SECURITY_ON));
        } else {
            mBtnSwitch.setSelected(false);
            sendBroadcast(new Intent(Define.ACTION.SECURITY_OFF));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
