package com.jiyinsz.smartlife;

import android.app.Application;
import android.content.Intent;

import com.jiyinsz.smartlife.pojo.Device;
import com.jiyinsz.smartlife.service.BluetoothLeService;
import com.jiyinsz.smartlife.service.Define;
import com.jiyinsz.smartlife.utils.ACache;

/**
 * Created by varx on 9/9/15.
 */
public class App extends Application {

    private static App instance;
    public static short sequenceId = 1;
    public static Device device = new Device();
    public static boolean isConnect = false;
    public static boolean isSecurityOn = false;
    public static boolean isBuleToothOn = false;
    public static String bindMac = "";

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        Device deviceInfo = (Device) ACache.get(getFilesDir()).getAsObject(Define.CACHE_KEY.DEVINFO);
        if (deviceInfo != null) {
            device = deviceInfo;
        }

        //蓝牙连接服务
        startService(new Intent(this, BluetoothLeService.class));

        //下载 OTA 文件服务
//        startService(new Intent(this, DownloadOta.class));
    }

    public static App getInstance() {
        return instance;
    }

}
