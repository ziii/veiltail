package com.jiyinsz.smartlife.db;

import android.database.sqlite.SQLiteDatabase;

import com.jiyinsz.smartlife.App;
import com.jiyinsz.smartlife.db.pojo.Setting;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by varx on 9/9/15.
 */
public class SettingManager {
    private static SQLiteDatabase database;
    private static Setting setting;

    static {
        SQLiteHelper helper = new SQLiteHelper(App.getInstance());
        database = helper.getWritableDatabase();
        setting = cupboard().withDatabase(database).query(Setting.class).get();
        if (setting == null) {
            setting = new Setting();
        }
    }

    public static String getBindMac() {
        return setting.mac;
    }

}
