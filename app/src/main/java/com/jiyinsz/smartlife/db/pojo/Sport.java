package com.jiyinsz.smartlife.db.pojo;

import nl.qbusict.cupboard.annotation.CompositeIndex;
import nl.qbusict.cupboard.annotation.Index;

/**
 * Created by varx on 9/18/15.
 */
public class Sport {
    public Long _id;
    @Index(uniqueNames = {@CompositeIndex(ascending = false, indexName = "sharedIndex")})
    public int date;
    @Index(uniqueNames = {@CompositeIndex(ascending = false, indexName = "sharedIndex", order = 1)})
    public int mins;
    public int value;
    public int step;
    public long addTime;
}
