package com.jiyinsz.smartlife.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.jiyinsz.smartlife.db.pojo.Setting;
import com.jiyinsz.smartlife.db.pojo.Sport;

import nl.qbusict.cupboard.CupboardBuilder;
import nl.qbusict.cupboard.CupboardFactory;

import static nl.qbusict.cupboard.CupboardFactory.cupboard;

/**
 * Created by varx on 9/9/15.
 */
public class SQLiteHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "veiltail.db";
    private static final int DATABASE_VERSION = 7;

    static {
        // register our models
        CupboardFactory.setCupboard(new CupboardBuilder().useAnnotations().build());
        cupboard().register(Setting.class);
        cupboard().register(Sport.class);
    }

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        cupboard().withDatabase(db).createTables();
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        cupboard().withDatabase(db).upgradeTables();
        if (oldVersion < 7) {
            db.execSQL("update Sport set value=value*64;");
            db.execSQL("update Sport set step=(value-1024)/4.3 where value>1024;");
        }
    }
}
