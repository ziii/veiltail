package com.jiyinsz.smartlife.wxapi;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.jiyinsz.smartlife.Loger;
import com.jiyinsz.smartlife.utils.WeiXinHelper;
import com.tencent.mm.sdk.modelbase.BaseReq;
import com.tencent.mm.sdk.modelbase.BaseResp;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.IWXAPIEventHandler;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

/**
 */
public class WXEntryActivity extends Activity implements IWXAPIEventHandler {


    private IWXAPI api;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //微信sdk
        api = WXAPIFactory.createWXAPI(this, WeiXinHelper.appId, false);
        api.registerApp(WeiXinHelper.appId);
        api.handleIntent(getIntent(), this);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        api.handleIntent(intent, this);
    }


    @Override
    public void onReq(BaseReq baseReq) {
        Loger.e(baseReq.toString());
    }

    @Override
    public void onResp(BaseResp baseResp) {
        if(baseResp.errCode== BaseResp.ErrCode.ERR_OK){
            Toast.makeText(this,"分享成功",Toast.LENGTH_SHORT).show();
        }
        finish();
    }
}
