package com.jiyinsz.smartlife.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jiyinsz.smartlife.BuildConfig;
import com.jiyinsz.smartlife.MainActivity;
import com.jiyinsz.smartlife.R;
import com.jiyinsz.smartlife.service.Define;
import com.testin.agent.TestinAgent;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences sp = getSharedPreferences(Define.SP.KEY, MODE_PRIVATE);
        int lastVer = sp.getInt(Define.SP.LAST_VER, 0);
        if (lastVer != BuildConfig.VERSION_CODE) {
            sp.edit().putInt(Define.SP.LAST_VER, BuildConfig.VERSION_CODE).apply();
            Intent intent = new Intent(this, InstructionActivity.class);
            intent.putExtra(InstructionActivity.ARG_HOME,true);
            startActivity(intent);
            finish();
            return;
        }
        setContentView(R.layout.activity_splash);
        TestinAgent.init(this, "147d04eda67ca8b7b00f4775bb6d0d01", "beta");
        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(SplashActivity.this, MainActivity.class));
                finish();
            }
        }, 1500);
    }

}
