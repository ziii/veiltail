package com.jiyinsz.smartlife.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.jiyinsz.smartlife.R;
import com.jiyinsz.smartlife.ble.DeviceManager;

public class TestActivity extends AppCompatActivity {

    private DeviceManager deviceManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);
        deviceManager = new DeviceManager(this);
    }

    public void connect(View v) {
        String mac = "C8:0A:B5:C1:13:B8";
        deviceManager.connect(mac);
    }

    public void sync(View v) {
        deviceManager.syncSport();
    }

    public void sleep(View v) {
        deviceManager.sleep();
    }

    public void battery(View v) {
        deviceManager.battery();
    }

    public void sn(View v) {
        deviceManager.sn();
    }

    public void firm(View v) {
        deviceManager.firm();
    }

    public void software(View v) {
        deviceManager.soft();
    }

    public void hareware(View v) {
        deviceManager.hard();
    }


    public void manu(View v) {
        deviceManager.manu();
    }

    public void type(View v) {
        deviceManager.type();
    }

    public void mac(View v) {
        deviceManager.mac();
    }

}
