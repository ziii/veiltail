package com.jiyinsz.smartlife.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.jiyinsz.smartlife.MainActivity;
import com.jiyinsz.smartlife.R;
import com.umeng.analytics.MobclickAgent;

import me.relex.circleindicator.CircleIndicator;

public class InstructionActivity extends AppCompatActivity {

    private boolean goHome = false;
    public static final String ARG_HOME = "ARG_HOME";
    private ImageView img1, img2, img3;

    private View mBtnFinish;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        goHome = getIntent().getBooleanExtra(ARG_HOME, false);
        setContentView(R.layout.activity_instruction);
        mBtnFinish = findViewById(R.id.btn_finish);

        ViewPager viewPager = (ViewPager) findViewById(R.id.viewpager);

        viewPager.setAdapter(new PagerAdapter() {

            @Override
            public void destroyItem(ViewGroup container, int position, Object object) {
                if (position == 0)
                    container.removeView(img1);
                if (position == 1)
                    container.removeView(img2);
                if (position == 2)
                    container.removeView(img3);
            }

            @Override
            public Object instantiateItem(ViewGroup container, int position) {
                if (position == 0) {
                    if (img1 == null) {
                        img1 = new ImageView(InstructionActivity.this);
                        img1.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        img1.setImageResource(R.mipmap.ins1);
                    }
//                    ViewGroup.LayoutParams layoutParams = new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                    container.addView(img1, 0);
                    return img1;
                }

                if (position == 1) {
                    if (img2 == null) {
                        img2 = new ImageView(InstructionActivity.this);
                        img2.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        img2.setImageResource(R.mipmap.ins2);
                    }
                    container.addView(img2, 0);
                    return img2;
                }
                if (position == 2) {
                    if (img3 == null) {
                        img3 = new ImageView(InstructionActivity.this);
                        img3.setScaleType(ImageView.ScaleType.FIT_CENTER);
                        img3.setImageResource(R.mipmap.ins3);
                    }
                    container.addView(img3, 0);
                    return img3;
                }
                return null;
            }

            @Override
            public int getCount() {
                return 3;
            }

            @Override
            public boolean isViewFromObject(View view, Object object) {
                return view == object;
            }
        });

        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
        indicator.setViewPager(viewPager);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 2) {
                    mBtnFinish.setVisibility(View.VISIBLE);
                } else {
                    mBtnFinish.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    public void onFinish(View v) {
        if (goHome) {
            startActivity(new Intent(this, MainActivity.class));
        }
        finish();
    }
}
