package com.jiyinsz.smartlife.ble;


import com.jiyinsz.smartlife.Loger;
import com.jiyinsz.smartlife.service.ByteUtils;
import com.jiyinsz.smartlife.service.Define;
import com.jiyinsz.smartlife.service.L1;
import com.jiyinsz.smartlife.service.L2;
import com.jiyinsz.smartlife.utils.CRC16Utils;

import java.nio.ByteBuffer;
import java.util.ArrayList;

/**
 * Created by varx on 10/9/15.
 */
public class Response {

    public byte[] seq;

    public int l2Length = 0;
    public int l2Complete = 0;

    //l2负载的长度
    public int valueLength = 0;
    private ArrayList<byte[]> mData = new ArrayList<>();

    //完整的L2数据
    public byte[] body = new byte[0];
    private L2 l2;
    private byte[] crc;

    public void onRead(byte[] data) {
        if (data == null || data.length == 0)
            return;
        mData.add(data);
        //L1
        if (data.length == 8 && data[0] == Define.MAGIC) {
            L1 l1 = L1.parse(data);
            crc = l1.crc;
            l2Length = l1.loadLength;
            seq = l1.seq;
        } else {
            onL2(data);
        }
    }

    public boolean isComplete() {
        return l2Complete == l2Length;
    }

    private void onL2(byte[] data) {
        l2Complete += data.length;
        if (l2 == null) {
            l2 = L2.parse(data);
            valueLength = l2.valueLength;
            body = l2.value;
            return;
        }
        if (body == null || body.length == 0) {
            body = data;
        } else {
            body = ByteUtils.concat(body, data);
        }
    }

    public boolean crc() {
        char crc1021 = CRC16Utils.getCRC1021(body, body.length);
        char wrap = ByteBuffer.wrap(crc, 0, 2).getChar();
        if (wrap == crc1021) {
            return true;
        } else {
            Loger.e("data :" + ByteUtils.toHumanString(body));
            Loger.e("crc error !cal crc :" + String.format("%H", crc1021) + " ,expect :" + ByteUtils.toHumanString(crc));
            return false;
        }
    }

    public void parse() {
        switch (l2.commandKey) {
            case Define.SPORT_KEY.RETURN_SPORT:
                Loger.e("运动数据");
                break;
            case Define.SPORT_KEY.STOP_SYNC_SPORT:
                Loger.e("完成运动数据");
                break;
        }
    }
}


