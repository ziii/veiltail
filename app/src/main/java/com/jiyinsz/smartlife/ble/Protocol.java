package com.jiyinsz.smartlife.ble;

import com.jiyinsz.smartlife.App;
import com.jiyinsz.smartlife.service.Define;
import com.jiyinsz.smartlife.utils.CRC16Utils;

import java.util.Calendar;
import java.util.UUID;

import static com.jiyinsz.smartlife.service.Define.*;

/**
 * Created by le on 2015/6/26.
 */
public class Protocol {

    //BLE设备的服务的UUID
    public final static String SERVICE_ACTIVITY_UUID = "000018aa";
    public final static String CHARACT_STATE_NOTIFY_UUID = "00000003";
    public final static String CHARACT_STATE_WRITE_UUID = "00000002";

    public final static String SERVICE_ACTIVITY_UUID_ALL = "000018aa-0000-1000-8000-00805f9b34fb";
    public final static String CHARACT_STATE_NOTIFY_UUID_ALL = "00000003-0000-1000-8000-00805f9b34fb";
    public final static String CHARACT_STATE_WRITE_UUID_ALL = "00000002-0000-1000-8000-00805f9b34fb";
    public final static String NOTIFICATION_DESCRIPTOR_UUID = "00002902-0000-1000-8000-00805f9b34fb";

    public final static byte HARDWARE_VERSION = 0x01;
    public final static byte FIRMWARE_VERSION = 0x02;
    public final static byte SOFTWARE_VERSION = 0x03;
    public final static byte SN_NUMBER = 0x04;
    public final static byte MANUFACTURER_NAME = 0x05;
    public final static byte DEVICE_TYPE = 0x08;
    public final static byte DEVICE_MAC = 0x09;

    public static class uuid {
        public final static UUID SERVICE = UUID.fromString(SERVICE_ACTIVITY_UUID_ALL);
        public final static UUID CHAR_WRITE = UUID.fromString(CHARACT_STATE_WRITE_UUID_ALL);

        public final static UUID CHAR_NOTIFY = UUID.fromString(CHARACT_STATE_NOTIFY_UUID_ALL);
        public final static UUID DES_NOTIFY = UUID.fromString(NOTIFICATION_DESCRIPTOR_UUID);

    }

    //0x20 读
    //0x40 写
    //0x80 notifi

    //根据L2生成L1
    public static byte[] createL1(byte[] L2) {
        byte[] L1 = new byte[8];
        L1[0] = (byte) 0xAB;
        L1[1] = 0; //00000000 其中高两位为保留位，3位表示传输正误（0正1误），4位表示应答标识位（0数据包，1应答包），后四位表示版本号
        L1[2] = (byte) ((short) L2.length >> 8);
        L1[3] = (byte) ((short) L2.length & 0x00ff);
        L1[4] = (byte) (CRC16Utils.getCRC1021(L2, L2.length) >> 8);
        L1[5] = (byte) (CRC16Utils.getCRC1021(L2, L2.length) & 0x00ff);
        L1[6] = (byte) (App.sequenceId >> 8);
        L1[7] = (byte) (App.sequenceId & 0x00ff);
        return L1;
    }

    //握手协议
    public static byte[] handShake() {

        byte[] L2;
        if (Define.DEV_JIYIN) {
            L2 = new byte[]{0x07, 0, 0x72, 0, 7, 0x40, 't', 'o', 'J', 'I', 'Y', 'N'};
        } else {
            L2 = new byte[]{0x07, 0, 0x72, 0, 2, 0x40, (byte) 0xA5};
        }
        return L2;
    }

    //BHYF握手协议
    public static byte[] handShakeBHYF() {
        byte[] L2 = {0x07, 0, 0x72, 0, 7, 0x40, 't', 'o', 'B', 'H', 'Y', 'F'};
        return L2;
    }

    //同步时间，时间为世界时间
    public static byte[] syscTime() {
        byte mYear, mMonth, mDay, mHour, mMinute, mSecond;
        String sYear, sMonth, sDay, sHour, sMinute, sSecond;
        final Calendar c = Calendar.getInstance();
        mYear = (byte) (c.get(Calendar.YEAR) % 100); //获取当前年份
        mMonth = (byte) (c.get(Calendar.MONTH) + 1);//获取当前月份
        mDay = (byte) c.get(Calendar.DAY_OF_MONTH);//获取当前月份的日期号码
        mHour = (byte) c.get(Calendar.HOUR_OF_DAY);//获取当前的小时数,24小时制
        mMinute = (byte) c.get(Calendar.MINUTE);//获取当前的分钟数
        mSecond = (byte) c.get(Calendar.SECOND);//获取当前秒数

        sYear = Integer.toBinaryString(mYear);
        sMonth = Integer.toBinaryString(mMonth);
        sDay = Integer.toBinaryString(mDay);
        sHour = Integer.toBinaryString(mHour);
        sMinute = Integer.toBinaryString(mMinute);
        sSecond = Integer.toBinaryString(mSecond);

        String s = getBitvalue(sYear, 6) + getBitvalue(sMonth, 4) + getBitvalue(sDay, 5) + getBitvalue(sHour, 5) + getBitvalue(sMinute, 6) + getBitvalue(sSecond, 6);
        String s1 = s.substring(0, 8);
        String s2 = s.substring(8, 16);
        String s3 = s.substring(16, 24);
        String s4 = s.substring(24, 32);
        byte[] time = new byte[4];
        time[0] = (byte) Short.parseShort(s1, 2);
        time[1] = (byte) Short.parseShort(s2, 2);
        time[2] = (byte) Short.parseShort(s3, 2);
        time[3] = (byte) Short.parseShort(s4, 2);
        byte[] value = {Define.CMD_ID.SRV, 0, 0x51, 0, 5, 0x40, time[0], time[1], time[2], time[3]};
        return value;
    }

    //读取时间
    public static byte[] readCurrentTime() {
        byte[] L2 = {0x07, 0, 0x51, 0, 1, 0x20};
        return L2;
    }

    //开启被窝温度notifi
    public static byte[] readTemp(byte on) {
        byte[] L2 = {0x07, 0, 0x31, 0, 2, (byte) 0x80, on};
        return L2;
    }

    //主动读取温度
    public static byte[] readTemp() {
        byte[] L2 = {0x07, 0, 0x31, 0, 1, (byte) 0x20};
        return L2;
    }

    //写应答包
    public static byte[] ack(boolean isAck, short sequenceId) {
        byte ack = isAck ? 0x10 : (byte) 0x30;
        byte[] L1 = new byte[8];
        L1[0] = (byte) 0xAB;
        L1[1] = ack; //00000000 其中高两位为保留位，3位表示传输正误（0正1误），4位表示应答标识位（0数据包，1应答包），后四位表示版本号
        L1[2] = 0;
        L1[3] = 0;
        L1[4] = 0;
        L1[5] = 0;
        L1[6] = (byte) (sequenceId >> 8);
        L1[7] = (byte) (sequenceId & 0x00ff);
        return L1;
    }

    public static byte[] ack(byte[] seq) {
        byte[] L1 = {
                Define.MAGIC,
                0b00010000,
                0x0,
                0x0,
                0x0,
                0x0,
                seq[0],
                seq[1]};
        return L1;
    }

    //读取硬件信息
    public static byte[] readDeviceInfo(byte type) {
        byte[] L2 = {
                Define.CMD_ID.SRV,
                0,          //4bits version+ 4bits reserved
                Define.SRV_KEY.INFO,
                0, 2,       //key header,7bits reserved+9 bits length
                Define.CMD_CODE.READ,
                type};
        return L2;
    }

    //读取电池电量
    public static byte[] readBatteryCapacity() {
        byte[] L2 = {Define.CMD_ID.SRV,
                0,//4bits version+ 4bits reserved
                Define.SRV_KEY.BATTERY,
                0, 1, //body length
                Define.CMD_CODE.READ};
        return L2;
    }

    //开始睡眠运动监测
    public static byte[] startSleepSport() {
        byte[] L2 = {CMD_ID.SPORT, 0, SPORT_KEY.START_MONITOR, 0, 0};
        return L2;
    }

    //同步睡眠数据
    public static byte[] readSleep() {
        byte[] L2 = {0x05, 0, 0x01, 0, 0};
        return L2;
    }

    //结束同步睡眠数据
    public static byte[] stopSyncSleep() {
        byte[] L2 = {0x05, 0, 0x06, 0, 0};
        return L2;
    }


//    public static byte[] start(){
//        byte[] l2= {
//                Define.CMD_ID.SPORT,    //command
//                0x00,       //version+reserved
//                Define.SPORT_KEY.START_SYNC_SPORT, //key
//                0x00,0x00   //2 bytes header;
//
//        };
//        return l2;
//    }

    //开始同步睡眠数据
    public static byte[] startSyncSleep() {
        byte[] L2 = {0x05, 0, 0x05, 0, 0};
        return L2;
    }

    //同步运动量数据
    public static byte[] readSport() {
        byte[] L2 = {0x05, 0, SPORT_KEY.SYNC_SPORT, 0, 0};
        return L2;
    }

    //结束同步运动数据
    public static byte[] stopSyncSport() {
        byte[] L2 = {0x05, 0, 0x08, 0, 0};
        return L2;
    }

    //开始同步运动数据
    public static byte[] startSyncSport() {
        byte[] L2 = {0x05, 0, 0x07, 0, 0};
        return L2;
    }

    /**
     * 设置防丢
     *
     * @param antiLost 0x10表示开启5m防丢，0x11表示10m防丢，0表示关闭防丢
     * @return
     */
    public static byte[] startAntiLost(byte antiLost) {
        byte[] L2 = {0x07, 0, 0x73, 0, 2, 0x40, antiLost};
        return L2;
    }

    //开启或者关闭踢被提醒
    public static byte[] openOrCloseKickQuilt(byte onOff) {
        byte[] L2 = {0x07, 0, 0x35, 0, 2, (byte) 0x80, onOff};
        return L2;
    }

    //根据str和len获取指定位数的字符串
    public static String getBitvalue(String str, int len) {
        if (len == str.length())
            return str;
        else {
            int length = len - str.length();
            for (int i = 0; i < length; ++i) {
                str = "0" + str;
            }

            return str;
        }
    }

    //将byte数组转为16进制
    public static String bytes2Hex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        String tmp = null;
        for (byte b : bytes) {
            // 将每个字节与0xFF进行与运算，然后转化为10进制，然后借助于Integer再转化为16进制
            tmp = Integer.toHexString(0xFF & b);
            if (tmp.length() == 1)// 每个字节8为，转为16进制标志，2个16进制位
            {
                tmp = "0" + tmp;
            }
            sb.append(tmp);
        }

        return sb.toString();

    }

    //L1和L2组包
    public static byte[] mergeL1L2(byte[] L1, byte[] L2) {

        byte[] L = new byte[L1.length + L2.length];
        System.arraycopy(L1, 0, L, 0, L1.length);
        System.arraycopy(L2, 0, L, L1.length, L2.length);
        return L;
    }

    //************解析数据包************
    //解析年月日
    public static String parseDate(String date) {
        int dateInt = Integer.parseInt(date, 16);
        String dateStr = Integer.toBinaryString(dateInt);
        int length = 15 - dateStr.length();
        for (int i = 0; i < length; i++) {
            dateStr = 0 + dateStr;
        }
        String year = dateStr.substring(0, 6);
        String month = dateStr.substring(6, 10);
        String day = dateStr.substring(10, 15);
        return Integer.parseInt(year, 2) + "-" + Integer.parseInt(month, 2) + "-" + Integer.parseInt(day, 2);
    }

    //解析时间XX点XX分
    public static String parseTime(String time) {
        int timeInt = Integer.parseInt(time, 16);
        int hour = timeInt / 60;
        int minute = timeInt % 60;
        return hour + ":" + minute;
    }


    /**
     * 计算睡眠时间
     *
     * @param data 睡眠状态数组
     *             return     计算出来的相应睡眠的时间，单位为min
     */
    public static int[] getSleepStatusTime(int[] data) {
        //浅睡眠时间
        int lightSleep = 0;
        //深睡眠时间
        int deepSleep = 0;
        for (int status : data) {
            if (status == 1)
                lightSleep++;
            else if (status == 2)
                deepSleep++;
        }
        int[] stateTime = {lightSleep * 15, deepSleep * 15};
        return stateTime;
    }

    /**
     * 解析运动数据包
     *
     * @param buffer 存储运动数据的缓冲数组
     * @return 返回的是存储解析后运动量的数组，一个数据表示半个小时内的运动量
     */
    public static int[] parseSportData(String[] buffer) {
        int[] cal = new int[buffer.length / 2];
        for (int i = 0; i < buffer.length / 2; i++) {
            cal[i] = Integer.parseInt(buffer[2 * i] + buffer[2 * i + 1], 16);
        }
        return cal;
    }

}
