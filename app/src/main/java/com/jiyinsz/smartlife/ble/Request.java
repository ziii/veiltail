package com.jiyinsz.smartlife.ble;

/**
 * Created by varx on 10/9/15.
 */
public class Request {
    private final byte[] cmd;
    public boolean hasAck = false;

    public void ack() {
        hasAck = true;
    }

    public Request(byte[] cmd) {
        this.cmd = cmd;
    }

    public byte[] getCmd() {
        return cmd;
    }


}
