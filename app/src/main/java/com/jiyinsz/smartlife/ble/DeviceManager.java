package com.jiyinsz.smartlife.ble;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;

import com.jiyinsz.smartlife.App;
import com.jiyinsz.smartlife.Loger;
import com.jiyinsz.smartlife.service.ByteUtils;
import com.jiyinsz.smartlife.service.Define;

import java.util.UUID;

/**
 * Created by varx on 10/9/15.
 */
public class DeviceManager {
    private final Context mContext;
    private final BluetoothAdapter mBluetoothAdapter;
    private BluetoothGatt mGatt;

    private boolean querying = false;
    private int mRetry = 3;

    public DeviceManager(Context context) {
        mContext = context;
        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
    }


    private void sendAck() {
        byte[] cmd = Protocol.ack(response.seq);
        writeCmd(cmd);
    }

    private Response response = new Response();
    private Request request;

    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Loger.e(String.format("onConnectionStateChange :%d,%d", status, newState));
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                gatt.discoverServices();
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            byte[] value = characteristic.getValue();
            Loger.e("onCharacteristicChanged：" + ByteUtils.toHumanString(value));
            boolean isAck = ((value[1] & Define.ACK_MASK) == Define.ACK_MASK);
            if (isAck)
                request.ack();
            else {
                response.onRead(value);
                if (response.isComplete()) {
                    sendAck();
                    response = new Response();
                }
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                byte[] value = characteristic.getValue();
                Loger.e("写入成功：" + ByteUtils.toHumanString(value) + " seq:" + App.sequenceId);
                byte cmdKey = ByteUtils.getCmdKey(value);
                switch (cmdKey) {
                    case Define.SRV_KEY.SHAKE_HANDS:
                        Loger.e("握手完成");
                        //握手没有 ack,写入成功即判断握手成功
                        //这里会开启 notify 以及启动设备的监测功能
                        enablNotify();
                        break;
                    default:
                        break;
                }
            } else {
                Loger.e("写入失败");
            }
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            if (status == gatt.GATT_SUCCESS) {
                Loger.e("connect success");
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Loger.e(String.format("onServicesDiscovered :%d", status));
            if (status == gatt.GATT_SUCCESS) {
                shakeHand();
            }
        }
    };

    public void syncSport() {
        byte[] cmd = Protocol.readSport();
        request = new Request(cmd);
        send();
    }

    private void send() {
        App.sequenceId++;
        byte[] l1 = Protocol.createL1(request.getCmd());
        writeCmd(ByteUtils.concat(l1, request.getCmd()));
    }

    private void writeCmd(byte[] value) {
        BluetoothGattService service = mGatt.getService(UUID.fromString(Protocol.SERVICE_ACTIVITY_UUID_ALL));
        BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(Protocol.CHARACT_STATE_WRITE_UUID_ALL));
        characteristic.setValue(value);
        mGatt.writeCharacteristic(characteristic);
    }

    private void enablNotify() {
        BluetoothGattService gattService = mGatt.getService(Protocol.uuid.SERVICE);
        BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(Protocol.uuid.CHAR_NOTIFY);
        mGatt.setCharacteristicNotification(gattCharacteristic, true);

        BluetoothGattDescriptor descriptor = gattCharacteristic.getDescriptor(Protocol.uuid.DES_NOTIFY);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mGatt.writeDescriptor(descriptor);
    }

    private void shakeHand() {
        BluetoothGattService service = mGatt.getService(UUID.fromString(Protocol.SERVICE_ACTIVITY_UUID_ALL));
        BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(Protocol.CHARACT_STATE_WRITE_UUID_ALL));
        byte[] cmd = Protocol.handShake();
        byte[] l1 = Protocol.createL1(cmd);
        byte[] value = ByteUtils.concat(l1, cmd);
        characteristic.setValue(value);
        boolean b = mGatt.writeCharacteristic(characteristic);
        Loger.e(String.valueOf(b));
    }

    public void connect(String mac) {
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(mac);
        mGatt = device.connectGatt(mContext, false, mGattCallback);
    }

    public void battery() {
        byte[] cmd = Protocol.readBatteryCapacity();
        request = new Request(cmd);
        send();
    }

    public void sn() {
        byte[] cmd = Protocol.readDeviceInfo(Define.DEV_INFO.SN_NUMBER);
        request = new Request(cmd);
        send();
    }

    public void firm() {
        byte[] cmd = Protocol.readDeviceInfo(Define.DEV_INFO.FIRMWARE_VERSION);
        request = new Request(cmd);
        send();
    }

    public void soft() {
        byte[] cmd = Protocol.readDeviceInfo(Define.DEV_INFO.SOFTWARE_VERSION);
        request = new Request(cmd);
        send();
    }

    public void hard() {
        byte[] cmd = Protocol.readDeviceInfo(Define.DEV_INFO.HARDWARE_VERSION);
        request = new Request(cmd);
        send();
    }

    public void manu() {
        byte[] cmd = Protocol.readDeviceInfo(Define.DEV_INFO.MANUFACTURER_NAME);
        request = new Request(cmd);
        send();
    }

    public void type() {
        byte[] cmd = Protocol.readDeviceInfo(Define.DEV_INFO.DEVICE_TYPE);
        request = new Request(cmd);
        send();
    }

    public void mac() {
        byte[] cmd = Protocol.readDeviceInfo(Define.DEV_INFO.DEVICE_MAC);
        request = new Request(cmd);
        send();
    }

    public void sleep() {
        byte[] cmd = Protocol.startSyncSleep();
        request = new Request(cmd);
        send();
    }


}
