package com.jiyinsz.smartlife.view;

import android.app.Activity;
import android.view.View;
import android.widget.TextView;

import com.jiyinsz.smartlife.R;
import com.jiyinsz.smartlife.utils.WeiXinHelper;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.ViewHolder;

/**
 * Created by varx on 5/21/15.
 */
public class ShareDialog implements View.OnClickListener {

    private Activity mContext;
    private OnShare listener;
    private DialogPlus mSahreDialog;
    private String title;

    public ShareDialog(Activity mContext) {
        this.mContext = mContext;
    }

    public void setCallback(OnShare listener) {
        this.listener = listener;
    }

    public void setTitle(String title) {
        this.title = title;
        if (mSahreDialog != null) {
            TextView tv = (TextView) mSahreDialog.findViewById(R.id.tv_title);
            tv.setText(title);
        }
    }

    public void show() {
        show(0);
    }

    public void show(int margin) {
        ViewHolder holder = new ViewHolder(R.layout.dialog_share);
        mSahreDialog = new DialogPlus.Builder(mContext)
                .setContentHolder(holder)
                .setCancelable(true)
                .setMargins(0, 0, 0, margin)
                .setGravity(DialogPlus.Gravity.BOTTOM)
                .create();
        mSahreDialog.findViewById(R.id.btn_share_circle).setOnClickListener(this);
        mSahreDialog.findViewById(R.id.btn_share_weichat).setOnClickListener(this);
        mSahreDialog.findViewById(R.id.btn_cancel).setOnClickListener(this);
        if (title != null) {
            TextView tv = (TextView) mSahreDialog.findViewById(R.id.tv_title);
            tv.setText(title);
        }
        mSahreDialog.show();
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_share_weichat:
                listener.wechat(getWeiXinHelper());
                break;
            case R.id.btn_share_circle:
                listener.circle(getWeiXinHelper());
                break;
            default:
                mSahreDialog.dismiss();
                break;
        }
        mSahreDialog.dismiss();
    }

    private WeiXinHelper getWeiXinHelper() {
        return new WeiXinHelper(mContext);
    }

    public interface OnShare {

        public void wechat(WeiXinHelper weiXinHelper);

        public void circle(WeiXinHelper weiXinHelper);
    }
}
