package com.jiyinsz.smartlife.service;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.jiyinsz.smartlife.Loger;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.MessageDigest;

public class DownloadOta extends IntentService {


    public DownloadOta() {
        super("DownloadOta");
    }

    //从 InputStream 读取到 "\r\n"为止,转换为大写字符串
    private String readLine(InputStream in) throws IOException {
        byte pre = 0;
        byte[] buffer = new byte[100];
        int index = 0;
        while (true) {
            byte read = (byte) in.read();
            if (pre == 0x0d && read == 0x0a) {
                break;
            }
            pre = read;
            buffer[index] = read;
            index++;
        }
        return new String(buffer, 0, index - 1).toUpperCase();
    }


    @Override
    protected void onHandleIntent(Intent intent) {
        SharedPreferences sp = getSharedPreferences(Define.SP.KEY, Context.MODE_PRIVATE);
        String localVersion = sp.getString(Define.SP.LOCAL_FW_VER, "");
        try {
            URL url = new URL(Define.OTA.URL_VER);
            String onlineVersion = url.openConnection().getHeaderField("version");

            //本地版本和线上一致,不下载
            if (TextUtils.equals(onlineVersion, localVersion))
                return;

            //下载最新的固件文件
            URL urlDownload = new URL(Define.OTA.URL_FILE);
            HttpURLConnection connection = (HttpURLConnection) urlDownload.openConnection();
            if (connection.getResponseCode() != connection.HTTP_OK) {
                return;
            }

            InputStream inputStream = connection.getInputStream();

            //第一行是 md5
            String md5OnLine = readLine(inputStream);
            Loger.e("line:" + md5OnLine);

            MessageDigest md = MessageDigest.getInstance("MD5");
            FileOutputStream fileOutputStream = openFileOutput(Define.OTA.FILE_NAME, MODE_PRIVATE);
            int bytesRead = -1;
            byte[] buffer = new byte[1024];
            while ((bytesRead = inputStream.read(buffer)) != -1) {
                //写入文件的同时计算 md5值
                md.update(buffer, 0, bytesRead);
                fileOutputStream.write(buffer, 0, bytesRead);
            }

            byte[] digest = md.digest();
            String digestReadable = ByteUtils.toHumanString(digest, false);
            if (!md5OnLine.equals(digestReadable)) {
                Loger.e("md5 check error! require: " + md5OnLine + " got:" + digestReadable);
                sp.edit().remove(Define.SP.LOCAL_FW_VER).apply();
            } else {
                sp.edit().putString(Define.SP.LOCAL_FW_VER, onlineVersion).apply();
            }
            fileOutputStream.close();
            inputStream.close();
            connection.disconnect();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

}
