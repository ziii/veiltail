package com.jiyinsz.smartlife.service;

import com.jiyinsz.smartlife.Loger;
import com.jiyinsz.smartlife.utils.CRC16Utils;

import java.nio.ByteBuffer;
import java.util.Arrays;

/**
 * Created by varx on 9/16/15.
 */
public class L2 {

    public byte[] data;
    public byte commandId;
    public byte commandKey;
    public int valueLength;
    public byte[] value;
    public boolean complete = false;

    private L2() {
    }

    /**
     * L2 结构:
     * 0:  commandId
     * 1:   4bits version+4bits reserved
     * 2:   commandKey
     * 3,4  7bits reserved+9bits length
     * ...  value,may be empty
     */

    public static L2 parse(byte[] dataL2) {

        L2 l2 = new L2();
        l2.data = dataL2;
        l2.commandId = dataL2[0];
        l2.commandKey = dataL2[2];
        l2.valueLength = ByteUtils.toInt(dataL2[3], dataL2[4]);
        if (l2.valueLength > 0) {
            l2.value = Arrays.copyOfRange(dataL2, 5, dataL2.length);
        }
        return l2;
    }

    public boolean crc(byte[] crc) {
        char crc1021 = CRC16Utils.getCRC1021(data, data.length);
        char wrap = ByteBuffer.wrap(crc, 0, 2).getChar();
        if (wrap == crc1021) {
            return true;
        } else {
            Loger.e("data :" + ByteUtils.toHumanString(data));
            Loger.e("crc error !cal crc :" + String.format("%H", crc1021) + " ,expect :" + ByteUtils.toHumanString(crc));
            return false;
        }
    }

    public int getLength() {
        return data.length;
    }

    public void append(byte[] value) {
        int aLen = data.length;
        int bLen = value.length;

        byte[] merge = new byte[aLen + bLen];

        System.arraycopy(data, 0, merge, 0, aLen);
        System.arraycopy(value, 0, merge, aLen, bLen);
        data = merge;
        this.value = Arrays.copyOfRange(data, 5, data.length);
    }
}
