package com.jiyinsz.smartlife.service;

import android.util.Log;

import java.nio.ByteBuffer;
import java.util.Calendar;

/**
 * Created by varx on 9/15/15.
 */
public class ByteUtils {

    private static final String TAG = "ByteUtils";


    public static byte[] concat(byte[] first, byte[] second) {
        byte[] result = new byte[first.length + second.length];
        System.arraycopy(first, 0, result, 0, first.length);
        System.arraycopy(second, 0, result, first.length, second.length);
        return result;
    }

    public static String toHumanString(byte[] data, boolean space) {
        if (data == null || data.length == 0)
            return "";
        String format;
        if (space) {
            format = "%02X ";
        } else {
            format = "%02X";
        }
        final StringBuilder stringBuilder = new StringBuilder(data.length);
        for (byte byteChar : data)
            stringBuilder.append(String.format(format, byteChar));
        return stringBuilder.toString();
    }

    public static boolean equalsStart(byte[] arr1, byte[] arr2) {
        int len = arr1.length;
        if (arr2.length < len)
            return false;
        for (int i = 0; i < len; i++) {
            if (arr1[i] != arr2[i])
                return false;
        }
        return true;
    }

    public static String toHumanString(byte[] data) {
        return toHumanString(data, true);
    }

    public static boolean isAck(byte[] data) {
        if (data[0] != Define.MAGIC)
            return false;
        //第四位是 ack 标志位
        byte ackMask = 0b00010000;
        if ((data[1] & 0b00010000) == ackMask) {
            return true;
        }
        return false;
    }

    //
    public static String parseTemp(byte[] data) {
        if (data.length != 4) {
            Log.e(TAG, "parseTemp length error");
            return "";
        }
        ByteBuffer wrap = ByteBuffer.wrap(data, 0, 2);
        ByteBuffer wrap1 = ByteBuffer.wrap(data, 2, 2);

        return String.format("%d.%d", wrap1.getShort(), wrap.getShort());
    }

    public static String parseDate(byte[] date) {
        if (date.length != 2)
            return "";
        int i = date[0] << 8 | date[1];
        int yearMask = 0b0111111000000000;
        int monMask = 0b0000000111100000;
        int dayMask = 0b0000000000011111;
        int year = (i & yearMask) >> 9;
        int mon = (i & monMask) >> 5;
        int day = i & dayMask;
        return String.format("%d-%d-%d", year, mon, day);
    }

    public static String parseTime(byte[] date) {
        int mins = toInt(date[0], date[1]);
        int hour = mins / 60;
        int min = mins % 60;
        return String.format("%2d:%2d", hour, min);
    }

    public static byte getCmdKey(byte[] data) {
        return data[10];
    }

    public static byte getL2CmdKey(byte[] data) {
        return data[2];
    }

    public static int getL2LoadLength(byte[] l2) {
        return l2[3] << 8 | l2[4];
    }

    public static int toInt(byte h, byte l) {
        return (h << 8 | (l & 0x00FF)) & 0xFFFF;
    }


    public static int todayInt() {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR) % 100;   //
        int month = calendar.get(Calendar.MONTH) + 1;
        int day = calendar.get(Calendar.DATE);
        int mask = year << 9 | month << 5 | day;
        return mask;
    }

    public static int[] parseDate(int date) {
        int yearMask = 0b0111111000000000;
        int monMask = 0b0000000111100000;
        int dayMask = 0b0000000000011111;
        int year = (date & yearMask) >> 9;
        int mon = (date & monMask) >> 5;
        int day = date & dayMask;
        int[] dateArr = new int[3];
        dateArr[0] = year;
        dateArr[1] = mon;
        dateArr[2] = day;
        return dateArr;
    }
}
