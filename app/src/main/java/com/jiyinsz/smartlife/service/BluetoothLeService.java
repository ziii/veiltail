package com.jiyinsz.smartlife.service;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.media.RingtoneManager;
import android.os.Binder;
import android.os.IBinder;
import android.support.v4.app.NotificationCompat;

import com.jiyinsz.smartlife.App;
import com.jiyinsz.smartlife.Loger;
import com.jiyinsz.smartlife.MainActivity;
import com.jiyinsz.smartlife.R;
import com.jiyinsz.smartlife.activity.SecurityDialog;
import com.jiyinsz.smartlife.ble.Protocol;
import com.jiyinsz.smartlife.db.SQLiteHelper;
import com.jiyinsz.smartlife.db.pojo.Sport;
import com.jiyinsz.smartlife.utils.ACache;

import java.util.ArrayList;
import java.util.Arrays;

import de.greenrobot.event.EventBus;

import static android.text.TextUtils.isEmpty;
import static nl.qbusict.cupboard.CupboardFactory.cupboard;

public class BluetoothLeService extends Service {
    private final static String TAG = BluetoothLeService.class.getSimpleName();


    public static final int STATE_DISCONNECTED = 0;

    public final static String EXTRA_DATA_RSSI = "com.doouya.bluetooth.le.EXTRA_DATA_RSSI";


    private BleManager mBleManager;
    private NotificationCompat.Builder mBuilder;


    private int notifyId = 23333;

    private void securityAlert() {
        mBuilder.setContentText("宝贝走丢了");
        long[] pattern = {500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500, 500};
        mBuilder.setVibrate(pattern);
        mBuilder.setSound(RingtoneManager.getActualDefaultRingtoneUri(this, RingtoneManager.TYPE_ALARM));
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(notifyId, mBuilder.build());
        //走丢地图
        Intent intent = new Intent(this, SecurityDialog.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }


    public class LocalBinder extends Binder {
        public BluetoothLeService getService() {
            return BluetoothLeService.this;
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        Loger.e("service onBind ");
        return mBinder;
    }

    @Override
    public void onDestroy() {
        Loger.e("onDestroy");
        mBleManager.disConnect();
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.cancel(notifyId);
        EventBus.getDefault().unregister(this);
        unregisterReceiver(mReceiver);

        super.onDestroy();
    }

    private final IBinder mBinder = new LocalBinder();


    private void setNotice(String text) {
        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        mBuilder.setContentText(text);
        mNotificationManager.notify(notifyId, mBuilder.build());

    }

    //gatt 连接之后的回调
    public void onConnected() {
        //开启监测功能
        mBleManager.appendCommand(Protocol.startSleepSport(), true);

        setNotice("设备已连接");
        readBattery();
        //读取运动数据
        mBleManager.appendCommand(Protocol.readSport(),
                new byte[]{Define.CMD_ID.SPORT, 0, Define.SPORT_KEY.STOP_SYNC_SPORT});
        Intent intent = new Intent();
        intent.setAction(Define.ACTION.DEVICE_CONNECT);
        App.isConnect = true;
        sendBroadcast(intent);

//        mBleManager.appendCommand(BabyHeroProtocol.readSleep());

    }

    public void onLostConnection() {
        setNotice("设备未连接");
        App.isConnect = false;
        Intent intent = new Intent();
        intent.setAction(Define.ACTION.DEVICE_DISCONNECT);
        sendBroadcast(intent);
        if (App.isSecurityOn) {
            securityAlert();
        }
    }

    private BroadcastReceiver mReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            showSecurityNotify();
        }
    };

    @Override
    public void onCreate() {
        super.onCreate();
        Loger.e("service created");
        mBleManager = new BleManager(this);
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Define.ACTION.SECURITY_OFF);
        intentFilter.addAction(Define.ACTION.SECURITY_ON);
        registerReceiver(mReceiver, intentFilter);
        EventBus.getDefault().register(this);

        Intent intent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);
        mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getResources().getString(R.string.app_name))
                .setContentText("设备未连接")
                .setOngoing(true).setContentIntent(pendingIntent);

        Notification notifycation = mBuilder.build();
        startForeground(notifyId, notifycation);
        /*   test   */
//        byte[] sportData = {0x1F, 0x33, 0x00, (byte) 0x96, 0x00, 0x04, 0x00, 0x04, 0x00, 0x04, 0x00, 0x04, 0x00, 0x05, 0x00, 0x05, 0x00, 0x04, 0x00, 0x05, 0x00, 0x05, 0x00, 0x05, 0x00, 0x05, 0x00, 0x05};
//        byte[] sportData={0x1F,0x2B,0x01, (byte) 0xFE,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05};
//        byte[] sportData={0x1F,0x37,0x00, (byte) 0xB4,0x00,0x08,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05,0x00,0x05};
//        parseSport(sportData);
    }

    public void scanAndBind(final String bindMac) {
        Loger.e("scanAndBind:" + bindMac);
        if (App.isConnect && App.bindMac.equals(bindMac)) {
            Loger.e("connected ,skip connect");
            return;
        }
        if (isEmpty(bindMac)) {
            mBleManager.scanAndBind(null);
        } else {
            mBleManager.scanAndBind(bindMac);
        }
    }

    public boolean isBtOn() {
        return mBleManager.isBtOn();
    }

    public void onEventMainThread(L2 l2) {
        onData(l2);
    }


    private void onData(L2 dataL2) {
        Loger.e("l2 load length:" + dataL2.valueLength);
        Intent intent;
        switch (dataL2.commandKey) {
            case Define.SRV_KEY.BATTERY:
                intent = new Intent();
                App.device.battery = dataL2.value[1];
                intent.setAction(Define.ACTION.BATTERY_UPDATE);
                sendBroadcast(intent);
                Loger.e("电池信息" + dataL2.value[1]);
                break;
            case Define.SPORT_KEY.RETURN_SPORT:
                Loger.e("返回运动数据:" + ByteUtils.toHumanString(dataL2.value));
                parseSport(dataL2.value);
                break;
            case Define.SPORT_KEY.STOP_SYNC_SPORT:
                Loger.e("同步运动数据结束");
                intent = new Intent();
                intent.setAction(Define.ACTION.STOP_SYNC);
                sendBroadcast(intent);
                if (App.device.isEmpty())
                    readDevInfo();
                break;
            case Define.SRV_KEY.INFO:
                parseDevInfo(dataL2.value);
                break;
        }
    }

    private void parseSport(byte[] sport) {
        SQLiteDatabase db = new SQLiteHelper(this).getWritableDatabase();
        int date = ByteUtils.toInt(sport[0], sport[1]);
        int mins = ByteUtils.toInt(sport[2], sport[3]);
        ArrayList<Sport> sports = new ArrayList<>();

        for (int i = 2; i < sport.length / 2; i++) {
            Sport sportBean = new Sport();
            //以当前时间为基准，每取的一个睡眠状态数据需要加30分钟
            if (i > 2)
                mins += 5;
            //如果超过24点，日期加一天，时间减一天
            if (mins / 60 >= 24) {
                date += 1;
                mins = mins - 24 * 60;
            }
            sportBean.date = date;
            sportBean.mins = mins;
            sportBean.value = ByteUtils.toInt(sport[2 * i], sport[2 * i + 1]) * 64;
//            sportBean.value+=1024;
            sportBean.step = sportBean.value > 1024 ? (int) ((sportBean.value - 1024) / 4.3) : 0;
            sportBean.addTime = System.currentTimeMillis();
            sports.add(sportBean);
        }
        cupboard().withDatabase(db).put(sports);
        db.close();
    }

    private void parseDevInfo(byte[] devInfo) {
        int end = devInfo.length;
        byte[] info = Arrays.copyOfRange(devInfo, 2, end);
        switch (devInfo[1]) {
            case Define.DEV_INFO.HARDWARE_VERSION:
                App.device.hardware_version = new String(info);
                Loger.e("硬件版本:" + new String(info));
                break;
            case Define.DEV_INFO.FIRMWARE_VERSION:
                App.device.firmware_version = new String(info);
                Loger.e("固件版本:" + new String(info));
                break;
            case Define.DEV_INFO.SOFTWARE_VERSION:
                App.device.software_version = new String(info);
                Loger.e("软件版本:" + new String(info));
                break;
            case Define.DEV_INFO.SN_NUMBER:
                App.device.sn_number = ByteUtils.toHumanString(info, false);
                Loger.e("SN:" + ByteUtils.toHumanString(info, false));
                saveDeviceCache();
                break;
            case Define.DEV_INFO.DEVICE_TYPE:
                App.device.device_type = new String(info);
                Loger.e("型号:" + ByteUtils.toHumanString(info));
                break;
            case Define.DEV_INFO.MANUFACTURER_NAME:
                App.device.manufacturer_name = new String(info);
                Loger.e("厂商:" + new String(info));
                break;
        }
    }

    private void saveDeviceCache() {
        ACache aCache = ACache.get(getFilesDir());
        aCache.put(Define.CACHE_KEY.DEVINFO, App.device);
    }

    public void readDevInfo() {
        byte[] finishMark = new byte[]{Define.CMD_ID.SRV, 0, Define.SRV_KEY.INFO};
        mBleManager.appendCommand(Protocol.readDeviceInfo(Define.DEV_INFO.HARDWARE_VERSION), finishMark);
        mBleManager.appendCommand(Protocol.readDeviceInfo(Define.DEV_INFO.FIRMWARE_VERSION), finishMark);
        mBleManager.appendCommand(Protocol.readDeviceInfo(Define.DEV_INFO.SOFTWARE_VERSION), finishMark);
        mBleManager.appendCommand(Protocol.readDeviceInfo(Define.DEV_INFO.SN_NUMBER), finishMark);
//        mBleManager.appendCommand(BabyHeroProtocol.readDeviceInfo(Define.DEV_INFO.DEVICE_TYPE));
//        mBleManager.appendCommand(BabyHeroProtocol.readDeviceInfo(Define.DEV_INFO.MANUFACTURER_NAME));

    }


    private void showSecurityNotify() {

        if (!App.isSecurityOn) {
            if (App.isConnect) {
                setNotice("设备已连接");
            } else {
                setNotice("设备未连接");
            }
        } else {
            setNotice("防丢保护中");
        }


    }

    public void readBattery() {
        mBleManager.appendCommand(Protocol.readBatteryCapacity(), new byte[]{Define.CMD_ID.SRV, 0, Define.SRV_KEY.BATTERY});
    }

    public void disconnect() {
        mBleManager.disConnect();
    }

    public void reconnect() {
        mBleManager.connectDevice(App.bindMac);
    }


}
