package com.jiyinsz.smartlife.service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by varx on 9/14/15.
 */
public class Group {
    private static int maxLength = 9;
    private static int minSpace = 2;

    public static void main(String[] args) {
        Integer[] pics = {1, 4, 5, 6, 10, 12, 40, 41, 42, 43, 45, 47, 48, 49, 50, 90, 91};

        ArrayList<ArrayList> result = new ArrayList<>();
        ArrayList<Integer> window = new ArrayList<>();
        for (int i = 0; i < pics.length; i++) {
            window.add(pics[i]);
            //下一元素间隔超出条件,截断
            if (i + 1 == pics.length || Math.abs(pics[i + 1] - pics[i]) > minSpace) {
                //截断的区间过长,再分割
                if (window.size() > maxLength) {
                    result.addAll(split(window));
                } else {
                    result.add(window);
                }
                window = new ArrayList<>();
            }
        }

        System.out.println(result);

    }


    private static ArrayList<ArrayList> split(List<Integer> in) {
        ArrayList<ArrayList> result = new ArrayList<>();
        int maxSpace = 0;
        int splitIndex = 0;
        for (int i = 1; i + 1 < in.size(); i++) {
            if (Math.abs(in.get(i) - in.get(i - 1)) >= maxSpace) {
                maxSpace = in.get(i) - in.get(i - 1);
                splitIndex = i;
            }
        }
        ArrayList<Integer> left = new ArrayList<Integer>(in.subList(0, splitIndex));
        if (left.size() < maxLength) {
            result.add(left);
        } else {
            result.addAll(split(left));
        }
        int size = in.size();
        ArrayList<Integer> right = new ArrayList<Integer>(in.subList(splitIndex, size));
        if (right.size() < maxLength) {
            result.add(right);
        } else {
            result.addAll(split(right));
        }
        return result;
    }


}
