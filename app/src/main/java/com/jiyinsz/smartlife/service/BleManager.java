package com.jiyinsz.smartlife.service;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.jiyinsz.smartlife.App;
import com.jiyinsz.smartlife.Loger;
import com.jiyinsz.smartlife.MainActivity;
import com.jiyinsz.smartlife.ble.Protocol;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.LinkedBlockingQueue;

import de.greenrobot.event.EventBus;

import static android.text.TextUtils.isEmpty;

/**
 * Created by varx on 9/15/15.
 * //管理命令队列
 * //与蓝牙设备通信
 * //组包
 */
public class BleManager {

    private final EventBus mEventBus;
    private boolean mRetryConnect = true;
    private int mRetryTime = 0;

    private LinkedBlockingQueue<Command> commandQueue = new LinkedBlockingQueue<>();
    private int contineOnAckSeq = 0;
    PackageController packageController = new PackageController();
    private ScanCallback mScanCallback;


    private class Command {
        public byte[] cmd;
        public boolean waitAck = false;
        public byte[] waitL2;
        public boolean finished = false;

        public Runnable callback;
        public int timeout = 3000;

        public Command(byte[] cmd, boolean waitAck) {
            this.waitAck = waitAck;
            this.cmd = cmd;
        }

        public Command(byte[] cmd, byte[] waitForHead) {
            this.waitAck = false;
            this.cmd = cmd;
            waitL2 = waitForHead;
            if (ByteUtils.equalsStart(waitForHead, new byte[]{Define.CMD_ID.SPORT, 0, Define.SPORT_KEY.STOP_SYNC_SPORT})) {
                //同步运动超时两分钟
                timeout = 120000;
            }
        }

        public void finish() {
            finished = true;
            Loger.e("finish :" + ByteUtils.toHumanString(cmd));
//            if (callback != null) {
//                mHandler.removeCallbacks(callback);
//                callback = null;
//            }

        }
    }

    private Handler mHandler = new Handler(Looper.getMainLooper());

    private final BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Loger.e("onConnectionStateChange " + newState);
            String intentAction;
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                //连接 gatt 成功以后,马上 discoverServices
                //在 discoverServices 以后进行握手
                boolean discoverServices = mGatt.discoverServices();
                Log.i(TAG, "Attempting to start service discovery:" + discoverServices);
                mHandler.removeCallbacks(runnableReconnect);
            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {
                mContext.onLostConnection();
                packageController.clear();
                if (mRetryConnect) {
                    Log.i(TAG, "Disconnected,retrying");
                    reConnectGatt();
                    mHandler.removeCallbacks(runnableReconnect);
                    mHandler.postDelayed(runnableReconnect, 5 * 1000);
                } else {
                    mGatt.close();
                }
            }
        }

        /**
         * 重连
         */
        private void reConnectGatt() {
            if (mBluetoothDevice != null) {
                if (mGatt != null) {
                    mGatt.close();
                    mGatt.disconnect();
                    mGatt = null;
                }
                mGatt = mBluetoothDevice.connectGatt(mContext, false, mGattCallback);
            } else {
                Log.e(TAG, "the bluetoothDevice is null, please reset the bluetoothDevice");
            }
        }

        Runnable runnableReconnect = new Runnable() {
            @Override
            public void run() {
                mHandler.postDelayed(this, 30 * 1000);
                reConnectGatt();
            }
        };

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Loger.e("onServicesDiscovered " + status);
            if (status != 0) {
                mBluetoothAdapter.disable();
                Timer singleTimer = new Timer();
                singleTimer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        mBluetoothAdapter.enable();
                    }
                }, 1000);
            }

            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(TAG, "写入握手信号");
                writeHandCharacteristic();
            } else {
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }


        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            byte[] value = characteristic.getValue();
            Loger.e("onCharacteristicChanged :" + ByteUtils.toHumanString(value));
            packageController.onRec(value);
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            Loger.e("onDescriptorWrite " + status);
            super.onDescriptorWrite(gatt, descriptor, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
                Loger.e("开始同步时间");
                syncTime();
            }
        }


        //指令写入完成之后回调
        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {

            if (status == BluetoothGatt.GATT_SUCCESS) {
                byte[] value = characteristic.getValue();
                Loger.e("写入成功：" + ByteUtils.toHumanString(value) + " seq:" + App.sequenceId);
                if (ByteUtils.isAck(value)) {
                    try {
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    packageController.unBlock();
//                    packageController.nextCommand();
                    return;
                }
//                App.sequenceId++;

                byte cmdKey = ByteUtils.getCmdKey(value);
                switch (cmdKey) {
                    case Define.SRV_KEY.SHAKE_HANDS:
                        //握手没有 ack,写入成功即判断握手成功
                        //这里会开启 notify 以及启动设备的监测功能
                        postHandShake();
                        break;
                    default:
                        break;
                }
            } else {
                Loger.e("写入失败");
            }
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            Loger.e("onReadRemoteRssi " + status + " rssi :" + rssi);
            super.onReadRemoteRssi(gatt, rssi, status);
            if (status == BluetoothGatt.GATT_SUCCESS) {
//                broadcastUpdate(ACTION_GATT_RSSI, rssi);
            }
        }
    };

    private int syncTimeSeqId = 0;

    private void syncTime() {

        syncTimeSeqId = App.sequenceId;

//        BluetoothGattService gattService = mGatt.getService(UUID.fromString(Protocol.SERVICE_ACTIVITY_UUID_ALL));
//        BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(UUID.fromString(Protocol.CHARACT_STATE_WRITE_UUID_ALL));
        byte[] l2 = Protocol.syscTime();
        byte[] value = Protocol.mergeL1L2(Protocol.createL1(l2), l2);
        writeCmd(value);
//        gattCharacteristic.setValue(value);
//        writeCharacteristic(gattCharacteristic);
    }

    private void writeCmd(byte[] value) {
        Loger.e("写入...:" + ByteUtils.toHumanString(value) + " seq:" + App.sequenceId);
        BluetoothGattService service = mGatt.getService(UUID.fromString(Protocol.SERVICE_ACTIVITY_UUID_ALL));
        if (service == null)
            return;
        BluetoothGattCharacteristic characteristic = service.getCharacteristic(UUID.fromString(Protocol.CHARACT_STATE_WRITE_UUID_ALL));
        characteristic.setValue(value);
        mGatt.writeCharacteristic(characteristic);
    }


    private void test() {
        ArrayList<byte[]> data = new ArrayList<>();
        data.add(new byte[]{(byte) 0xAB, 0x00, 0x00, 0x21, (byte) 0x8D, (byte) 0xB8, 0x00, (byte) 0x81});
        data.add(new byte[]{0x05, 0x00, 0x04, 0x00, 0x1C, 0x1F, 0x32, 0x05, 0x64, 0x00, 0x03, 0x00, 0x10, 0x00, 0x10, 0x00, 0x11, 0x00, 0x10, 0x00});
        data.add(new byte[]{0x11, 0x00, 0x10, 0x00, 0x10, 0x00, 0x10, 0x00, 0x10, 0x00, 0x10, 0x00, 0x10});
        data.add(new byte[]{(byte) 0xAB, 0x00, 0x00, 0x21, (byte) 0xC1, 0x2A, 0x00, (byte) 0x82});
        data.add(new byte[]{0x05, 0x00, 0x04, 0x00, 0x1C, 0x1F, 0x33, 0x01, 0x2C, 0x00, 0x10, 0x00, 0x10, 0x00, 0x10, 0x00, 0x10, 0x00, 0x10, 0x00});
        data.add(new byte[]{0x10, 0x00, 0x10, 0x00, 0x31, 0x00, 0x11, 0x00, 0x11, 0x00, 0x12, 0x00, 0x15});

        for (byte[] row : data) {
            packageController.onRec(row);
        }
    }

    public void disConnect() {
        mRetryConnect = false;
        Loger.e("disconect");
        packageController.clear();
        App.isSecurityOn = false;
        if (mGatt != null) {
            mGatt.close();
            mGatt.disconnect();
        }
        if (mScanCallback != null) {
            mBluetoothAdapter.stopLeScan(mScanCallback);
            mScanCallback = null;
        }
        mContext.onLostConnection();

    }


    private class PackageController {
        private boolean blocking = true;
        private Command cmd;
        private L1 l1;
        private L2 l2;

        private CountDownLatch lock;
        private DispatchThread thread = new DispatchThread();
        ;

        public void onRec(byte[] data) {
            if (data.length == 8 && data[0] == Define.MAGIC) {
                onL1(data);
            } else {
                onL2(data);
            }
        }

        public void unBlock() {
            blocking = false;
        }


        private class DispatchThread extends Thread {
            @Override
            public void run() {
                while (true) {
                    try {
                        Thread.sleep(200);
                        cmd = commandQueue.take();
                        App.sequenceId++;
                        byte[] l1 = Protocol.createL1(cmd.cmd);
                        final byte[] value = ByteUtils.concat(l1, cmd.cmd);
                        writeCmd(value);

                        lock = new CountDownLatch(1);
                        lock.await();
                        lock = null;
//                        cmd.callback = new Runnable() {
//                            @Override
//                            public void run() {
//                                if (!cmd.finished) {
//                                    Loger.e("time out ,resend");
//                                    writeCmd(value);
//                                }
//                            }
//                        };
//                        mHandler.postDelayed(cmd.callback, cmd.timeout);

                    } catch (InterruptedException e) {
                        e.printStackTrace();
                        break;
                    }
                }
            }
        }


        public void clear() {
            thread.interrupt();
            commandQueue.clear();
            thread = new DispatchThread();
        }

        public void onL1(byte[] data) {
            l1 = L1.parse(data);
            int seq = ByteUtils.toInt(l1.seq[0], l1.seq[1]);

            if (l1.isAck) {
                if (seq == syncTimeSeqId) {
                    blocking = false;
                    mContext.onConnected();
                    thread.start();
//                    nextCommand();
                    return;
                }
                Loger.e("read Ack");
                if (cmd.waitAck) {
                    cmd.finish();
                    lock.countDown();
                }

//                mHandler.removeCallbacks(cmd.callback);
//                if (!cmd.hasL2) {
//                    blocking = false;
//                    nextCommand();
//                }
            } else if (l1.loadLength == 0) {
                blocking = false;
//                nextCommand();
            }
            Intent intent;

            intent = new Intent(mContext, MainActivity.class);
            intent.setComponent(new ComponentName(mContext, MainActivity.class));
            intent.setClass(mContext, MainActivity.class);
        }


        private void onL2(byte[] data) {

            //第一个包
            if (l2 == null) {
                l2 = L2.parse(data);

            } else { //需要组包
                l2.append(data);
            }

            if (l1.loadLength == l2.getLength()) {
                l2.complete = true;
            }

            if (l2.complete) {
                l2.crc(l1.crc);
                //回复 ack
                writeCmd(Protocol.ack(l1.seq));
//                readOrWriteCharacteristic(Protocol.ack(l1.seq), true);
                try {
                    Thread.sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                //处理数据
                mEventBus.post(l2);
                this.l2 = null;
                if (ByteUtils.equalsStart(cmd.waitL2, data)) {
                    cmd.finish();
                    lock.countDown();
                }
//                blocking = false;
//                nextCommand();
            }
//            if(data[0]==0x05&&data[2]==0x08){
//                Loger.e("同步结束,开始测试");
//                test();
//            }
        }
    }


    /**
     * @param cmd     指令
     * @param waitAck 是否等待 L2 段
     */
    public void appendCommand(byte[] cmd, boolean waitAck) {
        Loger.e("appendCommand: " + ByteUtils.toHumanString(cmd) + " waitAck " + waitAck);
        Command command = new Command(cmd, waitAck);
        commandQueue.add(command);
//        nextCommand();
    }

    /**
     * @param cmd     指令
     * @param l2Start 等待匹配的l2头部
     */
    public void appendCommand(byte[] cmd, byte[] l2Start) {
        Loger.e("appendCommand: " + ByteUtils.toHumanString(cmd) + " wait cmd " + ByteUtils.toHumanString(l2Start));
        Command command = new Command(cmd, l2Start);
        commandQueue.add(command);
//        nextCommand();
    }


//    private void nextCommand() {
//        packageController.nextCommand();
//    }

    private final BluetoothAdapter mBluetoothAdapter;
    private final BluetoothLeService mContext;
    private String TAG = "BleManager";
    private String mBluetoothDeviceAddress;
    private BluetoothDevice mBluetoothDevice;
    private BluetoothGatt mGatt;
    private String deviceName = Define.DEV_JIYIN ? "JIYIN" : "babyhero";

//    private String deviceName = "dytest";

    public BleManager(BluetoothLeService context) {
        this.mContext = context;
        mEventBus = EventBus.getDefault();
        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
    }

    public boolean isBtOn() {
        return mBluetoothAdapter.isEnabled();
    }

    public void scanAndBind(final String macAddr) {
        Loger.e("start scaning" + macAddr);
        Intent intent = new Intent();
        intent.setAction(Define.ACTION.DEVICE_CONNECTING);
        mContext.sendBroadcast(intent);
        mScanCallback = new ScanCallback(macAddr);
        mBluetoothAdapter.startLeScan(mScanCallback);
    }


    private class ScanCallback implements BluetoothAdapter.LeScanCallback {
        private int maxRssi = -1000;
        private String maxMac = "";
        private long startTime = System.currentTimeMillis();

        private String targetMac;

        public ScanCallback(String targetMac) {
            this.targetMac = targetMac;
        }

        private void connect(final String addr) {
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    Loger.e("connecting to " + addr);
                    connectDevice(addr);
                }
            }, 100);
        }

        private void stopScan() {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    mBluetoothAdapter.stopLeScan(ScanCallback.this);
                    mScanCallback = null;
                }
            });
        }

        @Override
        public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
            String deviceName = device.getName();
            boolean timeouot = System.currentTimeMillis() - startTime > 10000;
//            if (!BleManager.this.deviceName.equals(deviceName))
//                return;
            Loger.e("find device  name: " + deviceName
                    + " mac:" + device.getAddress()
                    + " rssi: " + rssi);

            //如果绑定设备,连接5s 内 rssi 最大的设备
            if (isEmpty(targetMac)) {
                if (rssi >= maxRssi && BleManager.this.deviceName.equals(deviceName)) {
                    maxRssi = rssi;
                    maxMac = device.getAddress();
                }
                if (timeouot) {
                    stopScan();
                    if (!isEmpty(maxMac)) {
                        final String bindMac = maxMac;
                        SharedPreferences.Editor edit = mContext.getSharedPreferences(Define.SP.KEY, Context.MODE_PRIVATE).edit();
                        edit.putString(Define.SP.BIND_MAC, bindMac).commit();
                        connect(bindMac);
                    }

                }
            } else {
                if (device.getAddress().equals(targetMac)) {
                    stopScan();
                    connect(targetMac);
                }
                if (timeouot) {
                    stopScan();
                    disConnect();
                }
            }

        }
    }

    ;

    public boolean connectDevice(final String address) {
        mRetryConnect = false;
        if (mBluetoothAdapter == null || address == null) {
            Log.w(TAG, "BluetoothAdapter not initialized or unspecified address.");
            return false;
        }


        App.bindMac = address;
        mBluetoothDevice = mBluetoothAdapter.getRemoteDevice(address);
        if (mBluetoothDevice == null) {
            Log.w(TAG, "Device not found.  Unable to connect.");
            return false;
        }
        // We want to directly connect to the device, so we are setting the autoConnect
        // parameter to false.
        mGatt = mBluetoothDevice.connectGatt(mContext, false, mGattCallback);

        Log.d(TAG, "Trying to create a new connection.");
        mBluetoothDeviceAddress = address;
        return true;
    }

    /**
     * Request a read on a given {@code BluetoothGattCharacteristic}. The read result is reported
     * asynchronously through the {@code BluetoothGattCallback#onCharacteristicRead(android.bluetooth.BluetoothGatt, android.bluetooth.BluetoothGattCharacteristic, int)}
     * callback.
     *
     * @param characteristic The characteristic to read from.
     */
    private void readCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mGatt.readCharacteristic(characteristic);
    }


    private void postHandShake() {
        //握手成功，开启Notification
        BluetoothGattService gattService = mGatt.getService(UUID.fromString(Protocol.SERVICE_ACTIVITY_UUID_ALL));
        BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(UUID.fromString(Protocol.CHARACT_STATE_NOTIFY_UUID_ALL));
        setCharacteristicNotification(gattCharacteristic, true);

        BluetoothGattDescriptor descriptor = gattCharacteristic.getDescriptor(UUID.fromString(Protocol.NOTIFICATION_DESCRIPTOR_UUID));
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        writeDescriptor(descriptor);


//        Loger.e("开始运动睡眠监测");
//        appendCommand(BabyHeroProtocol.startSleepSport(), false);


    }


    /**
     * Enables or disables notification on a give characteristic.
     *
     * @param characteristic Characteristic to act on.
     * @param enabled        If true, enable notification.  False otherwise.
     */
    public void setCharacteristicNotification(BluetoothGattCharacteristic characteristic, boolean enabled) {
        if (mBluetoothAdapter == null || mGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mGatt.setCharacteristicNotification(characteristic, enabled);

        // This is specific to Heart Rate Measurement.
        //        if (UUID_HEART_RATE_MEASUREMENT.equals(characteristic.getUuid())) {
        //            BluetoothGattDescriptor descriptor = characteristic.getDescriptor(
        //                    UUID.fromString(SampleGattAttributes.CLIENT_CHARACTERISTIC_CONFIG));
        //            descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        //            mGatt.writeDescriptor(descriptor);
        //        }
    }


    /**
     * 对指定uuid读、写命令操作
     *
     * @param cmd   需要读写操作的命令
     * @param isAck 是否为应答命令，是为true
     */

    public void readOrWriteCharacteristic(byte[] cmd, boolean isAck) {
        if (mGatt == null) {
            return;
        }
        BluetoothGattService gattService = mGatt.getService(UUID.fromString(Protocol.SERVICE_ACTIVITY_UUID_ALL));
        if (gattService == null) {
            return;
        }
        BluetoothGattCharacteristic characteristic = gattService.getCharacteristic(UUID.fromString(Protocol.CHARACT_STATE_WRITE_UUID_ALL));
        byte[] value;
        if (isAck) {
            value = cmd;
        } else {
            value = Protocol.mergeL1L2(Protocol.createL1(cmd), cmd);
        }
        Loger.e("写入...:" + ByteUtils.toHumanString(value) + " seq:" + App.sequenceId);
        characteristic.setValue(value);
        writeCharacteristic(characteristic);
    }


    public void writeCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mBluetoothAdapter == null || mGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }

        mGatt.writeCharacteristic(characteristic);
    }


    public void writeDescriptor(BluetoothGattDescriptor descriptor) {
        if (mBluetoothAdapter == null || mGatt == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }
        mGatt.writeDescriptor(descriptor);
    }

    /**
     * 握手认证
     *
     * @return
     */
    public byte[] writeHandCharacteristic() {
        BluetoothGattService gattService = mGatt.getService(UUID.fromString(Protocol.SERVICE_ACTIVITY_UUID_ALL));
        BluetoothGattCharacteristic characteristic = gattService.getCharacteristic(UUID.fromString(Protocol.CHARACT_STATE_WRITE_UUID_ALL));
        byte[] value;
        value = Protocol.mergeL1L2(Protocol.createL1(Protocol.handShake()), Protocol.handShake());
        characteristic.setValue(value);
        writeCharacteristic(characteristic);
        App.sequenceId++;
        return value;
    }
}
