package com.jiyinsz.smartlife.service;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.jiyinsz.smartlife.Loger;
import com.jiyinsz.smartlife.ble.Protocol;
import com.jiyinsz.smartlife.eventBus.ConnectDevice;

import java.util.UUID;

import de.greenrobot.event.EventBus;

import static android.text.TextUtils.isEmpty;
import static com.jiyinsz.smartlife.service.Define.*;

/**
 */
public class DeviceCtrl {


    private final BluetoothAdapter mAdapter;
    private final Context mContext;
    private BluetoothDevice mDevice;
    private BluetoothGatt mGatt;
    private boolean mRetry = true;


    public DeviceCtrl(Context context) {
        mContext = context;
        EventBus.getDefault().register(this);
        BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        mAdapter = bluetoothManager.getAdapter();

    }


    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
        }
    };


    public void onEventMainThread(ConnectDevice event) {
        connectDevice(event.device);
    }


    @Override
    protected void finalize() throws Throwable {
        EventBus.getDefault().unregister(this);
        super.finalize();
    }

    private String TAG = "DeviceCtl";
    private BluetoothGattCallback mGattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Loger.e("onConnectionStateChange " + newState);

            //连接成功
            if (newState == BluetoothProfile.STATE_CONNECTED) {
                mGatt.discoverServices();
            } else {

                Loger.e("connect Lost");
                connectDevice(mDevice);
            }
        }


        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);

        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            Loger.e("onServicesDiscovered " + status);

            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d(TAG, "写入握手信号");
                writeHandCharacteristic();
            } else {
                //TODO service 发现失败
                Log.w(TAG, "onServicesDiscovered received: " + status);
            }
        }
    };


    private void enableNotify(){
        BluetoothGattService gattService = mGatt.getService(UUID.fromString(Protocol.SERVICE_ACTIVITY_UUID_ALL));
        BluetoothGattCharacteristic gattCharacteristic = gattService.getCharacteristic(UUID.fromString(Protocol.CHARACT_STATE_NOTIFY_UUID_ALL));
        mGatt.setCharacteristicNotification(gattCharacteristic, true);

        BluetoothGattDescriptor descriptor = gattCharacteristic.getDescriptor(UUID.fromString(Protocol.NOTIFICATION_DESCRIPTOR_UUID));
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        mGatt.writeDescriptor(descriptor);
    }

    private void connectDevice(BluetoothDevice device) {
        if (mAdapter == null) {
            return;
        }
        mDevice = device;
        mGatt = mDevice.connectGatt(mContext, false, mGattCallback);
    }

    /**
     * @param macAddr 如果 mac 地址空,则扫描并绑定一个新设备
     */
    public void scanOrConnect(final String macAddr) {
        if (isEmpty(macAddr)) {
            mContext.sendBroadcast(new Intent(ACTION.DEVICE_SCANING));
        } else {
            mContext.sendBroadcast(new Intent(ACTION.DEVICE_CONNECTING));
        }

        final String searchDevName = Define.DEV_JIYIN ? "jiyin" : "babyhero";
        mAdapter.startLeScan(new BluetoothAdapter.LeScanCallback() {

            private int maxRssi = -1000;
            private String maxMac = "";
            private long startTime = System.currentTimeMillis();

            @Override
            public void onLeScan(BluetoothDevice device, int rssi, byte[] scanRecord) {
                if (!searchDevName.equals(device.getName())) {
                    return;
                }

                //如果未绑定设备,连接5s 内 rssi 最大的设备
                if (isEmpty(macAddr)) {
                    if (rssi >= maxRssi) {
                        maxRssi = rssi;
                        maxMac = device.getAddress();
                    }
                    if (System.currentTimeMillis() - startTime > 5000) {
                        mAdapter.stopLeScan(this);
                        String bindMac = maxMac;
                        SharedPreferences.Editor edit = mContext.getSharedPreferences(Define.SP.KEY, Context.MODE_PRIVATE).edit();
                        edit.putString(Define.SP.BIND_MAC, bindMac).commit();
                        EventBus.getDefault().post(new ConnectDevice(device));
                        mAdapter.stopLeScan(this);
                    }
                }
                if (device.getAddress().equals(macAddr)) {
                    EventBus.getDefault().post(new ConnectDevice(device));
                    Loger.e("connecting to " + device.getAddress());
                    mAdapter.stopLeScan(this);
                }

                Loger.e(device.getName());
            }
        });
    }

    public void writeCharacteristic(BluetoothGattCharacteristic characteristic) {
        if (mAdapter == null || mAdapter == null) {
            Log.w(TAG, "BluetoothAdapter not initialized");
            return;
        }

        mGatt.writeCharacteristic(characteristic);
    }

    /**
     * 握手认证
     *
     * @return
     */
    public byte[] writeHandCharacteristic() {
        BluetoothGattService gattService = mGatt.getService(UUID.fromString(Protocol.SERVICE_ACTIVITY_UUID_ALL));
        BluetoothGattCharacteristic characteristic = gattService.getCharacteristic(UUID.fromString(Protocol.CHARACT_STATE_WRITE_UUID_ALL));
        byte[] value;
        value = Protocol.mergeL1L2(Protocol.createL1(Protocol.handShake()), Protocol.handShake());
        characteristic.setValue(value);
        writeCharacteristic(characteristic);
        return value;
    }

}
