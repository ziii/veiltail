package com.jiyinsz.smartlife.service;

import java.util.Arrays;

/**
 * Created by varx on 9/16/15.
 */
public class L1 {
    public boolean isAck = false;
    public int loadLength = 0;
    public byte[] seq;
    public byte[] crc;

    private L1() {
    }

    /**
     * L1共8字节,各字节含义:
     * 0:   magic byte
     * 1:   0,1bit 保留,2bit err flag,3 ack flag ,4567bits version
     * 2,3: L2长度
     * 4,5: crc16
     * 6,7: seqid
     */


    public static L1 parse(byte[] dataL1) {
        if (dataL1.length != 8 || dataL1[0] != Define.MAGIC)
            return null;
        L1 l1 = new L1();
        if ((dataL1[1] & Define.ACK_MASK) == Define.ACK_MASK) {
            l1.isAck = true;
        }
        l1.crc = Arrays.copyOfRange(dataL1, 4, 6);
        l1.loadLength = ByteUtils.toInt(dataL1[2], dataL1[3]);
        l1.seq = Arrays.copyOfRange(dataL1, 6, 8);
        return l1;
    }
}

