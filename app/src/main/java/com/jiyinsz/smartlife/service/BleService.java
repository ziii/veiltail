package com.jiyinsz.smartlife.service;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;

import com.jiyinsz.smartlife.Loger;
import com.jiyinsz.smartlife.ble.DeviceManager;
import com.jiyinsz.smartlife.ble.Request;
import com.jiyinsz.smartlife.ble.Response;

import java.util.LinkedList;

import de.greenrobot.event.EventBus;

public class BleService extends Service {
    private final LocalBinder mBinder;
    LinkedList<Request> queue = new LinkedList<>();
    private DeviceManager deviceManager;

    public BleService() {
        mBinder = new LocalBinder();
    }

    @Override
    public void onCreate() {
        super.onCreate();
        deviceManager = new DeviceManager(this);
        EventBus.getDefault().register(this);
    }

    @Override
    public void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }

    @Override
    public IBinder onBind(Intent intent) {
        Loger.e("service onBind ");
        return mBinder;
    }

    public void syncSport() {

    }

    public void readDeviceInfo() {

    }

    public void scanAndBind(String mac) {

    }

    public void onEventMainThread(Response response) {
        queryNext();
    }

    private void queryNext() {
        if (queue.isEmpty()) {
            return;
        }
        Request request = queue.removeLast();

    }

    public void scan() {
    }


    public class LocalBinder extends Binder {
        public BleService getService() {
            return BleService.this;
        }
    }
}
