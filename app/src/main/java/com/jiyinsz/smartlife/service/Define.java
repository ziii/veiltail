package com.jiyinsz.smartlife.service;

/**
 * Created by varx on 9/15/15.
 */
public class Define {
    public static final byte MAGIC = (byte) 0xAB;
    public static final byte ACK_MASK = 0b00010000;

    public static final boolean DEV_JIYIN = true;

    public static final class OTA {
        public static final String FILE_NAME = "dy_ther2.hex";
        public static final String URL_FILE = "http://fileupload.doouya.net/uploaded/official/dy_ther2.hex";
        public static final String URL_VER = "http://fileupload.doouya.net/version?owner=official";
    }

    public static final class CACHE_KEY {
        public static final String DEVINFO = "DEVINFO";

    }

    public static final class SP {
        public static final String KEY = "sharedPreference";
        public static final String BIND_MAC = "binded_mac";
        public static final String LOCAL_FW_VER = "LOCAL_FW_VER";
        public static final String LAST_VER = "LAST_VER";


    }

    public static final class ACTION {
        public static final String SECURITY_ON = "SECURITY_ON";
        public static final String SECURITY_OFF = "SECURITY_OFF";
        public static final String BATTERY_UPDATE = "BATTERY_UPDATE";
        public static final String DEVICE_CONNECT = "DEVICE_CONNECT";
        public static final String DEVICE_SCANING = "DEVICE_SCANING";
        public static final String DEVICE_DISCONNECT = "DEVICE_DISCONNECT";
        public static final String DEVICE_CONNECTING = "DEVICE_CONNECTING";
        public static final String STOP_SYNC = "STOP_SYNC";
        public static final String EXIT_ACTIVITY = "EXIT_ACTIVITY";

    }

    public static final class CMD_ID {
        public static final byte SPORT = 0x05;
        public static final byte TEST = 0x06;
        public static final byte SRV = 0x07;
    }

    //运动相关命令
    public static final class SPORT_KEY {
        public static final byte SYNC_SLEEP = 0x01;
        public static final byte RETURN_SLEEP = 0x02;
        public static final byte SYNC_SPORT = 0x03;
        public static final byte RETURN_SPORT = 0x04;
        public static final byte START_SYNC_SLEEP = 0x05;
        public static final byte STOP_SYNC_SLEEP = 0x06;
        public static final byte START_SYNC_SPORT = 0x07;
        public static final byte STOP_SYNC_SPORT = 0x08;
        //开始睡眠运动监测
        public static final byte START_MONITOR = 0x09;
        public static final byte STOP_MONITOR = 0x0A;
    }

    //unused
    //工厂测试相关 key
    public static final class TEST_KEY {

    }

    //
    public static final class CMD_CODE {
        public static final byte NOTIFY = (byte) 0b10000000;
        public static final byte WRITE = 0b01000000;
        public static final byte READ = 0b00100000;
    }

    //服务相关 key
    public static final class SRV_KEY {
        public static final byte TX_POW = 0x11;
        public static final byte BATTERY = 0x21;
        public static final byte BATTERY_REPORT_INTERVAL = 0x22;
        public static final byte TEMP = 0x31;
        public static final byte TEMP_REPORT_INTERVAL = 0x32;
        public static final byte TEMP_TYPE = 0x33;
        public static final byte INFO = 0x41;
        public static final byte SYNC_TIME = 0x51;
        public static final byte ACC = 0x61;
        public static final byte BLACK_LIGHT = 0x71;
        public static final byte SHAKE_HANDS = 0x72;
        public static final byte LOCK = 0x73;
        public static final byte IMMEDIATE_ALERT = (byte) 0x81;
        public static final byte LINK_LOSS_SERVICE = (byte) 0x91;
        public static final byte IMMEDIATE_ALERT_CLIENT = (byte) 0xA1;
    }

    //获取设备信息的信息类型
    public static final class DEV_INFO {
        public final static byte HARDWARE_VERSION = 0x01;
        public final static byte FIRMWARE_VERSION = 0x02;
        public final static byte SOFTWARE_VERSION = 0x03;
        public final static byte SN_NUMBER = 0x04;
        public final static byte MANUFACTURER_NAME = 0x05;
        public final static byte DEVICE_TYPE = 0x08;
        public final static byte DEVICE_MAC = 0x09;
    }
}