package com.jiyinsz.smartlife;

import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.jiyinsz.smartlife.db.SQLiteHelper;
import com.jiyinsz.smartlife.service.BluetoothLeService;
import com.jiyinsz.smartlife.service.ByteUtils;
import com.jiyinsz.smartlife.service.Define;
import com.umeng.analytics.MobclickAgent;
import com.umeng.update.UmengUpdateAgent;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private BluetoothLeService mBleService;
    private final ServiceConnection mBleServiceConnection = new ServiceConnection() {

        @Override
        public void onServiceConnected(ComponentName componentName, IBinder service) {
            Loger.e("onServiceConnected");
            BluetoothLeService.LocalBinder localBinder = (BluetoothLeService.LocalBinder) service;
            mBleService = localBinder.getService();
//            String mac = "C8:0A:B5:C1:13:B8";
//            String mac =null;
            connectDevice();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            Loger.e("onServiceDisconnected");
        }
    };
    private TextView mTextConnect;
    private TextView mTextBattery;
    private TextView mBtnSecurity;
    private TextView mTextSecurity;
    private TextView mTextSteps;
    private ImageView mImgRun;
    private boolean mIsBoundService;

    @Override
    protected void onStart() {
        super.onStart();

        //启动service
        Intent intent = new Intent(this, BluetoothLeService.class);
        mIsBoundService = bindService(intent, mBleServiceConnection, 0);
        Loger.e("bindService: " + mIsBoundService);

        readTodayData();
        updateSecurity();
        if (App.isConnect) {
            mTextConnect.setSelected(true);
            mTextConnect.setText("已连接");
            mTextBattery.setText(App.device.battery + "%");
        }
    }

    private void readTodayData() {
        SQLiteHelper sqLiteHelper = new SQLiteHelper(this);
        SQLiteDatabase database = sqLiteHelper.getReadableDatabase();
        int todayInt = ByteUtils.todayInt();
        Cursor cursor = database.rawQuery("select sum(step) from Sport where date=" + todayInt, null);
        int value = 0;
        if (cursor.moveToFirst()) {
            value = cursor.getInt(0);
        }
        int steps = value;
        mTextSteps.setText(String.format("今日步行了%d步", steps));
        sqLiteHelper.close();
    }

    private void connectDevice() {
        String mac = readBindMac();
        App.bindMac = mac;
        if (ensureBtOn())
            mBleService.scanAndBind(mac);
//        SharedPreferences.Editor edit = getSharedPreferences(Define.SP.KEY, Context.MODE_PRIVATE).edit();
//        edit.putString(Define.SP.BIND_MAC, mac).commit();
    }


    @Override
    protected void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
        try {
            unbindService(mBleServiceConnection);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void goSport(View v) {
        Intent intent = new Intent(this, SportActivity.class);
        startActivity(intent);
    }

    public void goSecurity(View v) {
        Intent intent = new Intent(this, SecurityActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    private void stopSyncAnim() {
        mImgRun.getAnimation().cancel();
        mImgRun.setImageResource(R.mipmap.run);
    }

    private void syncAnim() {
        mImgRun.setImageResource(R.mipmap.sync_green);

        RotateAnimation animation = new RotateAnimation(0f, 359f, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
        animation.setRepeatCount(animation.INFINITE);
        animation.setDuration(2000);
        animation.setInterpolator(new LinearInterpolator());
        mImgRun.startAnimation(animation);
//        mImgRun.getAnimation().start();
//        mImgRun.setImageResource(R);
    }

    private final BroadcastReceiver mBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals(Define.ACTION.DEVICE_CONNECT)) {
                syncAnim();
                mTextConnect.setText("已连接");
                mTextConnect.setSelected(true);
            } else if (intent.getAction().equals(Define.ACTION.DEVICE_DISCONNECT)) {
                mTextConnect.setText("未连接");
                mTextConnect.setSelected(false);
                mTextBattery.setText("0%");
            } else if (intent.getAction().equals(Define.ACTION.DEVICE_CONNECTING)) {
                mTextConnect.setText("连接中");
                mTextConnect.setSelected(true);
            } else if (intent.getAction().equals(Define.ACTION.BATTERY_UPDATE)) {
                mTextBattery.setText(App.device.battery + "%");
            } else if (intent.getAction().equals(Define.ACTION.SECURITY_ON)) {
                updateSecurity();
            } else if (intent.getAction().equals(Define.ACTION.SECURITY_OFF)) {
                updateSecurity();
            } else if (intent.getAction().equals(Define.ACTION.STOP_SYNC)) {
                stopSyncAnim();
                readTodayData();
            } else if (intent.getAction().equals("android.bluetooth.adapter.action.STATE_CHANGED")) {

                if (intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, -1) == BluetoothAdapter.STATE_OFF) {
                    App.isBuleToothOn = false;
                    mBleService.disconnect();
                    sendBroadcast(new Intent(Define.ACTION.DEVICE_DISCONNECT));
                    Toast.makeText(MainActivity.this, "蓝牙未打开,请打开蓝牙", Toast.LENGTH_SHORT).show();
                } else {
                    if (!App.isBuleToothOn && mBleService != null) {
                        new android.os.Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                connectDevice();
                            }
                        }, 2000);
                    }
                    App.isBuleToothOn = true;
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mTextConnect = (TextView) findViewById(R.id.text_connection_status);
        mTextBattery = (TextView) findViewById(R.id.text_battery);
        mBtnSecurity = (TextView) findViewById(R.id.btn_security);
        mBtnSecurity.setOnClickListener(this);
        mTextSecurity = (TextView) findViewById(R.id.text_security);
        mTextSteps = (TextView) findViewById(R.id.text_steps);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        mImgRun = (ImageView) findViewById(R.id.image_run);


        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("未养");
        checkBlueToothSupport();

        startService(new Intent(this, BluetoothLeService.class));


        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(Define.ACTION.BATTERY_UPDATE);
        intentFilter.addAction(Define.ACTION.DEVICE_CONNECT);
        intentFilter.addAction(Define.ACTION.DEVICE_DISCONNECT);
        intentFilter.addAction(Define.ACTION.DEVICE_CONNECTING);
        intentFilter.addAction(Define.ACTION.SECURITY_ON);
        intentFilter.addAction(Define.ACTION.SECURITY_OFF);
        intentFilter.addAction(Define.ACTION.STOP_SYNC);
        intentFilter.addAction("android.bluetooth.adapter.action.STATE_CHANGED");
        registerReceiver(mBroadcastReceiver, intentFilter);

        UmengUpdateAgent.update(this);

    }


    private String readBindMac() {
        SharedPreferences sp = getSharedPreferences(Define.SP.KEY, Context.MODE_PRIVATE);
        return sp.getString(Define.SP.BIND_MAC, "");
    }

    private void checkBlueToothSupport() {
        boolean feature = getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE);
        if (!feature) {
            return;
        }
        BluetoothManager bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        BluetoothAdapter adapter = bluetoothManager.getAdapter();
        if (adapter.isEnabled()) {
            App.isBuleToothOn = true;
        } else {
            adapter.enable();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingActivity.class);
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void updateSecurity() {
        if (App.isSecurityOn) {
            mBtnSecurity.setText("开");
            mTextSecurity.setText("防丢开启");
        } else {
            mBtnSecurity.setText("关");
            mTextSecurity.setText("防丢关闭");
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(mBroadcastReceiver);
        mBleService = null;
        if (App.isSecurityOn) {
            sendBroadcast(new Intent(Define.ACTION.EXIT_ACTIVITY));
        } else {
            Loger.e("stopService");
            stopService(new Intent(this, BluetoothLeService.class));
        }
    }

    private boolean ensureBtOn() {
        if (!mBleService.isBtOn()) {
            Toast.makeText(MainActivity.this, "蓝牙未打开,请先打开蓝牙", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void showConnectPopup(View v) {
        if (App.isConnect) {
            new AlertDialog.Builder(this).setTitle("确定断开连接?").setMessage("断开后可使用设备测温").setPositiveButton("确定", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    mBleService.disconnect();
                }
            }).setNegativeButton("取消", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            }).create().show();
            return;
        }
        if (!ensureBtOn())
            return;
        connectDevice();
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_connect);
        dialog.findViewById(R.id.btn_cancel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        dialog.show();
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.btn_security) {
            if (!App.isConnect && !App.isSecurityOn) {
                Toast.makeText(this, "设备未连接", Toast.LENGTH_SHORT).show();
            } else {
                App.isSecurityOn = !App.isSecurityOn;
                if (App.isSecurityOn)
                    sendBroadcast(new Intent(Define.ACTION.SECURITY_ON));
                else
                    sendBroadcast(new Intent(Define.ACTION.SECURITY_OFF));
            }
        }
    }
}
