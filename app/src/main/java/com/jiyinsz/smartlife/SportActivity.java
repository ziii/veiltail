package com.jiyinsz.smartlife;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.jiyinsz.smartlife.db.SQLiteHelper;
import com.jiyinsz.smartlife.service.ByteUtils;
import com.jiyinsz.smartlife.utils.MiscUtils;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

import static com.jiyinsz.smartlife.R.id.text_steps;

public class SportActivity extends AppCompatActivity {

    private int mTodayInt;
    private View mBtnNext;
    private View mBtnPre;
    private TextView mTextDate;
    private TextView mTextSteps;
    private TextView mTextDistance;
    private TextView mTextTitle;

    private ArrayList<DailySport> sports;
    private View mStar2, mStar3, mStar4, mStar5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sport);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("运动量");

        mBtnNext = findViewById(R.id.btn_next);
        mBtnPre = findViewById(R.id.btn_pre);
        mTextDate = (TextView) findViewById(R.id.text_date);
        mTextTitle = (TextView) findViewById(R.id.text_title);

        mTextSteps = (TextView) findViewById(text_steps);
        mTextDistance = (TextView) findViewById(R.id.text_distance);

        mStar2 = findViewById(R.id.star_2);
        mStar3 = findViewById(R.id.star_3);
        mStar4 = findViewById(R.id.star_4);
        mStar5 = findViewById(R.id.star_5);

        mTodayInt = ByteUtils.todayInt();
        readData();
    }

    private int index = 0;

    private void readData() {
        SQLiteHelper sqLiteHelper = new SQLiteHelper(this);
        SQLiteDatabase database = sqLiteHelper.getReadableDatabase();
        Cursor cursor = database.rawQuery("select date,sum(step) from Sport group by date order by date desc;", null);
        sports = new ArrayList<>();
        if (cursor.moveToFirst()) {
            do {
                DailySport sport = new DailySport();
                sport.setdate(cursor.getInt(0));
                sport.setValue(cursor.getInt(1));
                sports.add(sport);
            } while (cursor.moveToNext());
        }
        database.close();
        mBtnNext.setVisibility(View.INVISIBLE);
        int values = 0;
        if (sports.size() > 0) {
            values = sports.get(0).value;
        }
        setValue(values);
        if (sports.size() < 2) {
            mBtnPre.setVisibility(View.INVISIBLE);
        }
    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }


    public void onPreDay(View V) {
        if (index + 1 == sports.size()) {
            return;
        }
        index++;
        if (index + 1 == sports.size()) {
            mBtnPre.setVisibility(View.INVISIBLE);
        }
        mBtnNext.setVisibility(View.VISIBLE);
        DailySport dailySport = sports.get(index);
        mTextDate.setText(dailySport.date);
        setValue(dailySport.value);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void setValue(int steps) {
//        int steps = MiscUtils.valueToStep(value);
        mTextSteps.setText(String.valueOf(steps));
        String format = String.format("步行了%.2f公里", steps * 0.7 / 1000);
        mTextDistance.setText(format);

        if (steps < 1000) {
            mTextTitle.setText("居家死宅");
        } else if (steps < 3000) {
            mTextTitle.setText("安静美男");
        } else if (steps < 5000) {
            mTextTitle.setText("活力适中");
        } else if (steps < 8000) {
            mTextTitle.setText("活力四射");
        } else {
            mTextTitle.setText("运动达人");
        }
        setStar(steps);
    }

    private void setStar(int steps) {
        mStar2.setVisibility(View.INVISIBLE);
        mStar3.setVisibility(View.INVISIBLE);
        mStar4.setVisibility(View.INVISIBLE);
        mStar5.setVisibility(View.INVISIBLE);

        if (steps >= 1000)
            mStar2.setVisibility(View.VISIBLE);
        if (steps >= 3000)
            mStar3.setVisibility(View.VISIBLE);
        if (steps >= 5000)
            mStar4.setVisibility(View.VISIBLE);
        if (steps >= 8000)
            mStar5.setVisibility(View.VISIBLE);
    }

    public void onNextDay(View v) {
        if (index == 0)
            return;
        index--;
        DailySport dailySport = sports.get(index);
        mTextDate.setText(dailySport.date);
        if (index == 0) {
            mBtnNext.setVisibility(View.INVISIBLE);
        }
        mBtnPre.setVisibility(View.VISIBLE);
        setValue(dailySport.value);
    }

    public void share(View v) {
        Intent intent = new Intent(this, ReportActivity.class);
        startActivity(intent);
    }

    private class DailySport {
        private String date;
        private int value;
        private int kilometre;

        public void setdate(int date) {
            if (date == mTodayInt) {
                this.date = "今日";
            } else {
                int[] parseDate = ByteUtils.parseDate(date);
                this.date = String.format("%2d月%2d日", parseDate[1], parseDate[2]);
            }
        }

        public void setValue(int value) {
            this.value = value;
        }
    }
}
