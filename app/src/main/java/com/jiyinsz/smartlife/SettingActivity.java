package com.jiyinsz.smartlife;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.jiyinsz.smartlife.activity.InstructionActivity;
import com.jiyinsz.smartlife.service.BluetoothLeService;
import com.jiyinsz.smartlife.service.Define;
import com.umeng.analytics.MobclickAgent;

import java.util.ArrayList;

import static android.text.TextUtils.isEmpty;


public class SettingActivity extends AppCompatActivity {


    private BaseAdapter mAdapter;
    private ArrayList<SettingItem> mItems;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("设置");
        ListView listView = (ListView) findViewById(R.id.listview);
        createView();
        mAdapter = new BaseAdapter() {
            @Override
            public int getCount() {
                return mItems.size();
            }

            @Override
            public Object getItem(int position) {
                return null;
            }

            @Override
            public long getItemId(int position) {
                return 0;
            }

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                return createItem(position, parent);
            }
        };
        listView.setAdapter(mAdapter);


    }

    private View.OnClickListener mOtaClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if (App.device.battery < 50) {
                Toast.makeText(SettingActivity.this, "电量不足50%", Toast.LENGTH_SHORT).show();
                return;
            }
            new AlertDialog.Builder(SettingActivity.this)
                    .setTitle("确定升级?")
                    .setMessage("升级过程中必须确保设备电量充足,蓝牙连接稳定.\n中途断开连接导致升级失败可能导致设备无法可用.")
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
//                            Intent intent = new Intent(SettingActivity.this, UpgradeActivity.class);
//                            startActivity(intent);
                        }
                    })
                    .show();

        }
    };
    private View.OnClickListener mHelpClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(SettingActivity.this, InstructionActivity.class));
        }
    };

    private View.OnClickListener mUnbindClick = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            new AlertDialog.Builder(SettingActivity.this)
                    .setTitle("确定解除绑定?")
                    .setMessage("解除绑定之后可以连接新设备,当前设备记录的数据将会丢失.")
                    .setPositiveButton("确定", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(SettingActivity.this, BluetoothLeService.class);
                            stopService(intent);
                            SharedPreferences.Editor edit = getSharedPreferences(Define.SP.KEY, MODE_PRIVATE).edit();
                            edit.remove(Define.SP.BIND_MAC).commit();
                            App.isConnect = false;
                            App.bindMac = "";
                            App.device.clear();
                            startService(intent);
                            createView();
                            mAdapter.notifyDataSetChanged();
                        }
                    })
                    .setNegativeButton("取消", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .show();
        }
    };

    private boolean canUpgrade() {
        SharedPreferences sp = getSharedPreferences(Define.SP.KEY, MODE_PRIVATE);
        String lastFw = sp.getString(Define.SP.LOCAL_FW_VER, null);
        if (isEmpty(lastFw) || isEmpty(App.device.software_version))
            return false;
        if (lastFw.equals(App.device.software_version)) {
            return false;
        }
        return true;
    }

    private class SettingItem {
        String title;
        String info;
        View.OnClickListener listener;

        public SettingItem(String title, String info, View.OnClickListener listener) {
            this.title = title;
            this.info = info;
            this.listener = listener;
        }
    }


    private void createView() {
        mItems = new ArrayList<>();
        SettingItem item;
        item = new SettingItem("设备绑定状态", null, null);
        if (!isEmpty(App.bindMac)) {
            item.listener = mUnbindClick;
            item.info = "设备已绑定";
        }
        mItems.add(item);

        mItems.add(new SettingItem("", "", null));

        mItems.add(new SettingItem("硬件版本", App.device.hardware_version, null));
        mItems.add(new SettingItem("固件版本", App.device.firmware_version, null));
        mItems.add(new SettingItem("软件版本", App.device.software_version, null));
        mItems.add(new SettingItem("sn", App.device.sn_number, null));
        if (App.device.isEmpty()) {
            mItems.add(new SettingItem("生产商", "", null));
            mItems.add(new SettingItem("设备型号", "", null));
        } else {
            mItems.add(new SettingItem("生产商", "JIYIN", null));
            mItems.add(new SettingItem("设备型号", "JY320-1", null));
        }
        mItems.add(new SettingItem("电量", String.valueOf(App.device.battery) + "%", null));
        mItems.add(new SettingItem("帮助", "", mHelpClick));


    }

    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    private View createItem(int pos, ViewGroup parent) {
        SettingItem menuItem = mItems.get(pos);
        if (isEmpty(menuItem.title)) {
            return getLayoutInflater().inflate(R.layout.setting_space, parent, false);
        }
        View item = getLayoutInflater().inflate(R.layout.setting_item, parent, false);
        TextView title = (TextView) item.findViewById(R.id.title);
        TextView summary = (TextView) item.findViewById(R.id.summary);
        title.setText(menuItem.title);
        summary.setText(menuItem.info);
        item.setOnClickListener(menuItem.listener);
        return item;

//        if (pos == 2) {
//            return getLayoutInflater().inflate(R.layout.setting_space, parent, false);
//        }
//        View item = getLayoutInflater().inflate(R.layout.setting_item, parent, false);
//        TextView title = (TextView) item.findViewById(R.id.title);
//        TextView summary = (TextView) item.findViewById(R.id.summary);
//        switch (pos) {
//            case 0:
//                title.setText("设备绑定状态");
//                if (!isEmpty(App.bindMac)) {
//                    item.setOnClickListener(mUnbindClick);
//                    summary.setText("设备已绑定");
//                }
//
//
//                break;
////            case 1:
////                title.setText("手动绑定设备");
////                break;
////            case 2:
////                title.setText("恢复出厂设置");
////                break;
//            case 1:
//                title.setText("OTA 升级");
//                if (canUpgrade() || BuildConfig.DEBUG) {
//                    summary.setText("升级可用");
//                    item.setOnClickListener(mOtaClick);
//                }
//                break;
//            case 3:
//                title.setText("硬件版本");
//                summary.setText(App.device.hardware_version);
//                break;
//            case 4:
//                title.setText("固件版本");
//                summary.setText(App.device.firmware_version);
//                break;
//            case 5:
//                title.setText("软件版本");
//                summary.setText(App.device.software_version);
//                break;
//            case 6:
//                title.setText("sn");
//                summary.setText(App.device.sn_number);
//                break;
//            case 7:
//                title.setText("生产商");
////                summary.setText(App.device.manufacturer_name);
//                summary.setText("JIYIN");
//                break;
//            case 8:
//                title.setText("设备型号");
////                summary.setText(App.device.device_type);
//                summary.setText("JY320-1");
//                break;
//            case 9:
//                title.setText("电量");
//                summary.setText(String.valueOf(App.device.battery) + "%");
//                break;
//            case 10:
//                title.setText("帮助");
//                item.setOnClickListener(mHelpClick);
//                break;
//        }
//        return item;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_setting, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_refresh) {
            return true;
        }

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
