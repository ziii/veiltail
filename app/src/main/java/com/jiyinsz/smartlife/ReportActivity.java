package com.jiyinsz.smartlife;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import com.jiyinsz.smartlife.db.SQLiteHelper;
import com.jiyinsz.smartlife.service.ByteUtils;
import com.jiyinsz.smartlife.utils.MiscUtils;
import com.jiyinsz.smartlife.utils.WeiXinHelper;
import com.jiyinsz.smartlife.view.ShareDialog;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.umeng.analytics.MobclickAgent;

import java.math.BigInteger;
import java.security.MessageDigest;

public class ReportActivity extends AppCompatActivity {
    private TextView mTextPercent;
    private TextView mTextSteps;
    private TextView mTextTotalSteps;
    private TextView mTextMaxSteps;


    private int mTodaySteps = 0;
    private long mMaxSteps = 0;
    private long mTotalSteps = 0;
    private TextView mTextTitle;
    private int mPercent = 0;

    private ImageView mStar1, mStar2, mStar3, mStar4, mStar5;
    private int level = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        mTextPercent = (TextView) findViewById(R.id.text_percent);
        mTextSteps = (TextView) findViewById(R.id.text_steps);
        mTextMaxSteps = (TextView) findViewById(R.id.text_max_steps);
        mTextTotalSteps = (TextView) findViewById(R.id.text_total_steps);
        mTextTitle = (TextView) findViewById(R.id.text_title);

        mStar1 = (ImageView) findViewById(R.id.star_1);
        mStar2 = (ImageView) findViewById(R.id.star_2);
        mStar3 = (ImageView) findViewById(R.id.star_3);
        mStar4 = (ImageView) findViewById(R.id.star_4);
        mStar5 = (ImageView) findViewById(R.id.star_5);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("运动量成绩单");

        readDate();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_report, menu);
        return true;
    }

    private void share() {
        String macMd5 = "";
        try {
            MessageDigest md = null;
            md = MessageDigest.getInstance("MD5");
            String mac = App.bindMac;
            mac = mac.replaceAll(":", "");
            byte[] macByte = new BigInteger(mac, 16).toByteArray();
            byte[] digest = md.digest(macByte);
            macMd5 = ByteUtils.toHumanString(digest, false);
        } catch (Exception e) {
            e.printStackTrace();
        }


        ShareDialog dialog = new ShareDialog(this);
        final String url = String.format("http://page.doouya.com/product/jiyin/sportshare?p=%d&d=%d&a=%d&w=%d&t=%d&mid=%s", mPercent, mMaxSteps, mTotalSteps, mTodaySteps, level, macMd5);
        Loger.e(url);
        dialog.setCallback(new ShareDialog.OnShare() {
            @Override
            public void wechat(WeiXinHelper weiXinHelper) {
                weiXinHelper.share(url, SendMessageToWX.Req.WXSceneSession);
            }

            @Override
            public void circle(WeiXinHelper weiXinHelper) {
                weiXinHelper.share(url, SendMessageToWX.Req.WXSceneTimeline);
            }
        });
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }
        if (id == R.id.action_share) {
            share();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void onResume() {
        super.onResume();
        MobclickAgent.onResume(this);
    }

    public void onPause() {
        super.onPause();
        MobclickAgent.onPause(this);
    }

    private void readDate() {
        SQLiteHelper sqLiteHelper = new SQLiteHelper(this);
        SQLiteDatabase database = sqLiteHelper.getReadableDatabase();
        int todayInt = ByteUtils.todayInt();
        Cursor cursor = database.rawQuery("select sum(step) from Sport where date=" + todayInt, null);

        if (cursor.moveToFirst()) {
            mTodaySteps = cursor.getInt(0);
        }

        cursor = database.rawQuery("select max(step) from (select date,sum(step) as step from Sport group by date);", null);
        if (cursor.moveToFirst()) {
            mMaxSteps = cursor.getInt(0);
        }

        cursor = database.rawQuery("select sum(step) as value from Sport ;;", null);
        if (cursor.moveToFirst()) {
            mTotalSteps = cursor.getInt(0);
        }

        mPercent = (int) (percent(mTodaySteps) * 100);
        database.close();

        if (mTodaySteps < 1000) {
            mTextTitle.setText("居家死宅");
            highStar(mStar1);
            darkStar(mStar2, mStar3, mStar4, mStar5);
            level = 1;
        } else if (mTodaySteps < 3000) {
            mTextTitle.setText("安静美男");
            highStar(mStar1, mStar2);
            darkStar(mStar3, mStar4, mStar5);
            level = 2;
        } else if (mTodaySteps < 5000) {
            mTextTitle.setText("活力适中");
            highStar(mStar1, mStar2, mStar3);
            darkStar(mStar4, mStar5);
            level = 3;
        } else if (mTodaySteps < 8000) {
            mTextTitle.setText("活力四射");
            highStar(mStar1, mStar2, mStar3, mStar4);
            darkStar(mStar5);
            level = 4;
        } else {
            mTextTitle.setText("运动达人");
            highStar(mStar1, mStar2, mStar3, mStar4, mStar5);
            level = 5;
        }
        String win = String.format("%d%%\n的用户", mPercent);
        Spannable wordtoSpan = new SpannableString(win);
        wordtoSpan.setSpan(new RelativeSizeSpan(2.1f), 0, win.indexOf("%"), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        mTextPercent.setText(wordtoSpan);


        mTextMaxSteps.setText(String.valueOf(mMaxSteps));
        mTextSteps.setText(String.valueOf(mTodaySteps));
        mTextTotalSteps.setText(String.valueOf(mTotalSteps));

    }


    private double percent(int step) {
        double overUserPercent = 0.00;
        if (step == 0) {
            overUserPercent = 0.00;
        } else if (step <= 100) {
            overUserPercent = 0.01;
        } else if (step <= 1000) {
            overUserPercent = ((step - 100) * 0.20 / 900 + 0.01);
        } else if (step <= 3000) {
            overUserPercent = (step - 1000) * 0.30 / 2000 + 0.21;
        } else if (step <= 5000) {
            overUserPercent = (step - 3000) * 0.30 / 2000 + 0.51;
        } else if (step <= 10000) {
            overUserPercent = (step - 5000) * 0.10 / 5000 + 0.81;
        } else if (step <= 30000) {
            overUserPercent = (step - 10000) * 0.05 / 20000 + 0.91;
        } else if (step <= 100000) {
            overUserPercent = (step - 30000) * 0.04 / 70000 + 0.96;
        } else if (step > 100000) {
            overUserPercent = 1.00;
        }
        return overUserPercent;
    }

    private void darkStar(ImageView... imgs) {
        int mainColor = Color.GRAY;
        for (ImageView img : imgs) {
            img.setColorFilter(mainColor, PorterDuff.Mode.SRC_IN);
        }
    }

    private void highStar(ImageView... imgs) {
        int mainColor = getResources().getColor(R.color.main_color);
        for (ImageView img : imgs) {
            img.setColorFilter(mainColor, PorterDuff.Mode.SRC_IN);
        }
    }

}
