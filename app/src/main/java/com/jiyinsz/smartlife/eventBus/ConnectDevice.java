package com.jiyinsz.smartlife.eventBus;

import android.bluetooth.BluetoothDevice;

/**
 * Created by varx on 9/27/15.
 */
public class ConnectDevice {
    public BluetoothDevice device;

    public ConnectDevice(BluetoothDevice device) {
        this.device = device;
    }
}
