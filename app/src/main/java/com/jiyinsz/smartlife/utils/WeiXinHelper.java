package com.jiyinsz.smartlife.utils;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.widget.Toast;

import com.jiyinsz.smartlife.Loger;
import com.jiyinsz.smartlife.R;
import com.tencent.mm.sdk.modelmsg.SendMessageToWX;
import com.tencent.mm.sdk.modelmsg.WXMediaMessage;
import com.tencent.mm.sdk.modelmsg.WXWebpageObject;
import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.WXAPIFactory;

public class WeiXinHelper {
    public static final String appId = "wx9f06c0b5d8b109cc";
    private static final String appSecret = "74f63e826eb1dd48130ccd669f57a411";


    private Context mContext;

    private IWXAPI api;

    public WeiXinHelper(Context context) {
        mContext = context;
        api = WXAPIFactory.createWXAPI(mContext, appId, false);
        if (!api.isWXAppInstalled()) {
            Toast.makeText(context, "未安装微信客户端", Toast.LENGTH_SHORT).show();
        } else {
            boolean app = api.registerApp(appId);
            Loger.e(String.valueOf(app));
        }
    }


    public void share(String url, int type) {
        WXWebpageObject webpageObject = new WXWebpageObject();
        webpageObject.webpageUrl = url;

        WXMediaMessage msg = new WXMediaMessage();
        msg.mediaObject = webpageObject;
        msg.title = "我的运动量成绩单";
        msg.setThumbImage(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.share_icon));

        SendMessageToWX.Req req = new SendMessageToWX.Req();
        req.transaction = String.valueOf(System.currentTimeMillis());
        req.message = msg;
        req.scene = type;
        api.sendReq(req);
    }


}
