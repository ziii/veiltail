package com.jiyinsz.smartlife.utils;

/**
 * Created by varx on 9/22/15.
 */
public class MiscUtils {
    public static int valueToStep(long value) {
        return value > 1024 ? (int) ((value - 1024) / 4.3) : 0;
    }
}
